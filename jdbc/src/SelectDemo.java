import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectDemo {
    public Connection getConnection() throws SQLException {
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL("jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf8&useSSL=false");
        ds.setUser("root");
        ds.setPassword("xiaobai520..@@@");
        Connection c = ds.getConnection();
        return c;
    }

    public void selectOne() throws SQLException {
        String sql = "select * from user";
        Connection c = getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            int id = rs.getInt("id");
            String name = rs.getString("name");
            System.out.println("id:"+id+" "+"name:"+name);
        }
        rs.close();
        ps.close();
        c.close();
    }

    public static void main(String[] args) throws SQLException {
        SelectDemo selectDemo = new SelectDemo();
        selectDemo.selectOne();
    }
}
