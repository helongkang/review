import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertOne {
    public static void main(String[] args) throws SQLException {
        //1.创建数据库源
        //DataSource ds = new MysqlDataSource(); //DataSource是接口，MysqlDataSource()是实现类
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL("jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf8&useSSL=false");
        ds.setUser("root");
        ds.setPassword("xiaobai520..@@@");
        //2.和数据库服务器建立网络连接
        Connection c = ds.getConnection();
        //String sql = "insert into user values(1,'琨琨')";
        String sql = "insert into user values(?,?)";
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1,2);
        ps.setString(2,"蔡徐坤");
        int n = ps.executeUpdate();
        System.out.println(n);
        ps.close();
        c.close();
    }
}
