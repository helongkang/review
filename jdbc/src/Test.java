import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        String[] arr = {"a","b","c"};
        List<String> list = Arrays.asList(arr);
        String[] newArr = Arrays.copyOf(arr,6);
        for(String s : newArr){
            System.out.println(s);
        }
    }
}
