package dao;

import model.Article;
import model.User;
import util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    public static User selectByUserName(String username){
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            c = DBUtil.getConnection();
            String sql = "select * from user where username=?";
            ps = c.prepareStatement(sql);
            ps.setString(1,username);
            rs = ps.executeQuery();
            while(rs.next()){
               User user = new User();
               user.setId(rs.getInt("id"));
               user.setUsername(rs.getString("username"));
               user.setPassword(rs.getString("password"));
               return user;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DBUtil.close(c,ps,rs);
        }
        return null;
    }
}
