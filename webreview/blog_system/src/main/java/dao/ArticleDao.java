package dao;

import model.Article;
import util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArticleDao {
    //新增一篇博客
    public static void insertOne(Article article){
        Connection c = null;
        PreparedStatement ps = null;
        try{
            c = DBUtil.getConnection();
            String sql = "insert into article values(null,?,?,?,?)";
            ps = c.prepareStatement(sql);
            ps.setString(1,article.getTitle());
            ps.setString(2,article.getContent());
            ps.setTimestamp(3,article.getSendTimeStamp());
            ps.setInt(4,article.getUserId());
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DBUtil.close(c,ps,null);
        }
    }

    //根据id查询博客（用于博客详情）
    public static Article selectById(Integer id){
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            c = DBUtil.getConnection();
            String sql = "select * from article where id=?";
            ps = c.prepareStatement(sql);
            ps.setInt(1,id);
            rs = ps.executeQuery();
            while(rs.next()){
                Article article = new Article();
                article.setId(rs.getInt("id")); //要和表字段对应
                article.setTitle(rs.getString("title"));
                article.setContent(rs.getString("content"));
                article.setSendTime(rs.getTimestamp("send_time"));
                article.setUserId(rs.getInt("user_id"));
                return article;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DBUtil.close(c,ps,rs);
        }
        return null;
    }

    //查询所有文章（用于博客列表）
    public static List<Article> selectAll(){
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Article> articles = new ArrayList<>();
        try{
            c = DBUtil.getConnection();
            String sql = "select * from article order by send_time desc";
            ps = c.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                Article article = new Article();
                article.setId(rs.getInt("id")); //要和表字段对应
                article.setTitle(rs.getString("title"));
                //博客列表的文章只显示了部分
                String content = rs.getString("content");
                if(content.length() > 50){
                    content = content.substring(0,50)+"...";
                }
                article.setContent(content);
                article.setSendTime(rs.getTimestamp("send_time"));
                article.setUserId(rs.getInt("user_id"));
                articles.add(article);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DBUtil.close(c,ps,rs);
        }
        return articles;
    }

    //删除博客
    public void deleteById(Integer id){

    }
}
