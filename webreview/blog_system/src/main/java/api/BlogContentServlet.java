package api;

import dao.ArticleDao;
import model.Article;
import util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/content")
public class BlogContentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf8");
        req.setCharacterEncoding("utf8");
        Map<String,Object> result = new HashMap<>();
        HttpSession session = req.getSession(false);
        if(session!=null && session.getAttribute("user")!=null){
            int id = Integer.parseInt(req.getParameter("id"));
            Article article = ArticleDao.selectById(id);
            result.put("data",article);
        }else {
            result.put("status",-1);
            result.put("msg","未登录不允许访问");
        }
        String json = WebUtil.mapper.writeValueAsString(result);
        resp.getWriter().write(json);
    }
}
