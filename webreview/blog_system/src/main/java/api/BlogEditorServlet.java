package api;

import dao.ArticleDao;
import model.Article;
import model.User;
import util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/edit")
public class BlogEditorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        HttpSession session = req.getSession(false);
        Map<String,Object> result = new HashMap<>();
        if(session == null){
            result.put("status",-1);
            result.put("msg","用户未登录");
        }else {
            if(session.getAttribute("user") == null){
                result.put("status",-1);
                result.put("msg","用户未登录");
            }else {
                result.put("status",200);
                result.put("msg","用户已登录");
            }
        }
        resp.setContentType("application/json; charset=utf8");
        String json = WebUtil.mapper.writeValueAsString(result);
        resp.getWriter().write(json);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        resp.setContentType("application/json; charset=utf8");
        Map<String,Object> result = new HashMap<>();
        Article article = WebUtil.mapper.readValue(req.getInputStream(),Article.class);
        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute("user");
        int userId = user.getId();
        article.setUserId(userId);
        article.setSendTime(new Timestamp(System.currentTimeMillis()));
        ArticleDao.insertOne(article);

        result.put("msg","发布文章成功");
        String json = WebUtil.mapper.writeValueAsString(result);
        resp.getWriter().write(json);
    }
}
