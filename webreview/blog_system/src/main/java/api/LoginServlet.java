package api;

import dao.UserDao;
import model.User;
import util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        User user = WebUtil.mapper.readValue(req.getInputStream(),User.class);
        String username = user.getUsername();
        String password = user.getPassword();
        User get = UserDao.selectByUserName(username);
        Map<String,Object> result = new HashMap<>();
        if(get == null){
            result.put("msg","用户名不存在");
            result.put("status",-1);
        }else {
            if(get.getPassword().equals(password)){
                result.put("msg","登录成功");
                result.put("status",200);
                HttpSession session = req.getSession(true);
                session.setAttribute("user",get);
            }else {
                result.put("msg","用户名或密码错误");
                result.put("status",-1);
            }
        }
        resp.setContentType("application/json; charset=utf8");
        String json = WebUtil.mapper.writeValueAsString(result);
        resp.getWriter().write(json);
    }
}
