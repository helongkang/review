package api;

import dao.ArticleDao;
import model.Article;
import model.User;
import util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/blog_list")
public class BlogListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String,Object> result = new HashMap<>();
        HttpSession session = req.getSession(false);
        if(session != null && (session.getAttribute("user")!=null)){
            List<Article> articles = ArticleDao.selectAll();
            result.put("status",200);
            result.put("data",articles);
        }else {
            result.put("status",-1);
            result.put("msg","用户未登录，不允许访问");
        }
        resp.setContentType("application/json; charset=utf-8");
        String json = WebUtil.mapper.writeValueAsString(result);
        resp.getWriter().write(json);
    }
}
