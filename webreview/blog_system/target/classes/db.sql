create database if not exists blog_system character set utf8mb4;
use blog_system;

create table user(
    id int primary key auto_increment,
    username varchar(20) unique,
    password varchar(20)
);

create table article(
    id int primary key auto_increment,
    title varchar(50),
    content varchar(4096),
    send_time datetime,
    user_id int
);
