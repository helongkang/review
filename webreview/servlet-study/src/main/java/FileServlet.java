import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;

@WebServlet("/file")
public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OutputStream ops = resp.getOutputStream(); //使用ops可以往body中写二进制文件
        //BufferedOutputStream bos = new BufferedOutputStream(ops);
        resp.setContentType("image/jpeg");
        File file = new File("D:\\photo\\18.jpg");
        byte[] bytes = Files.readAllBytes(file.toPath());
        ops.write(bytes);
    }
}
