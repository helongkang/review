package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

@Controller
public class StudentController {
    @Autowired
    private Student student;

    @PostConstruct
    public void printStu(){
        System.out.println(student);
    }
}
