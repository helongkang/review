package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class TestHello {

//    @Value("${name}")
//    private String name;
//    @Value("${age}")
//    private int age;

    private static final Logger log = LoggerFactory.getLogger(TestHello.class);
    @RequestMapping("/hello")
    public String hello(){
        log.trace("Im trace");
        log.info("Im info");
        return "hello spring boot";
    }
}
