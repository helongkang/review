package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;

@Controller
public class ListController {
    @Autowired
    private TestList testList;


    @PostConstruct
    public void printList(){
        System.out.println(testList);
    }
}
