package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class TestYml {
    @Value("${str1}")
    private String str1;
    @Value("${str2}")
    private String str2;
    @Value("${str3}")
    private String str3;
    @Value("${num}")
    private int num;
    @Value("${flag1}")
    private boolean flag1;
    @Value("${flag2}")
    private boolean flag2;
    @Value("${pai}")
    private double pai;
    @Value("${node}")
    private String node;

    //该注解代表构造方法，初始化bean的时候会调用该方法
    @PostConstruct
    public void construct(){
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        System.out.println(num);
        System.out.println(pai);
        System.out.println(flag1);
        System.out.println(flag2);
        System.out.println(node);
    }
}
