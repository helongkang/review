package com.example.demo.controller;


import com.example.demo.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
//@ResponseBody //表示返回的是数据，不是页面
//@RestController //@Controller + @ResponseBody

public class UserController {
    //@RequestMapping(value = "/hello",method = RequestMethod.POST) //注册接口映射

    //@GetMapping("/hello")
//    @PostMapping("/hello")
//    public String printHello(){
//        return "hello Spring MVC";
//    }

//    @RequestMapping("/test")
//    public String print(String username){
//        return "hello "+username;
//    }

//    @RequestMapping("/login")
//    public String login(String username,String password){
//        return "login success "+username+" "+password;
//    }

//    @RequestMapping("/test")
//    public String print(){
//        return "<p>hh</p>";
//    }

//    @RequestMapping("/reg")
//    public Object reg(User user){
//        return user;
//    }

//    @RequestMapping("/login")
//    public String login(@RequestParam("username") String name,@RequestParam(value = "password",required = false) String word){
//        return name+word;
//    }

//    @RequestMapping("/login")
//    public Object login(@RequestBody User user){
//        return user;
//    }

    //获取带层次的资源路径
    @RequestMapping("/login/{username}/{password}")
    public String login(@PathVariable String username,@PathVariable(required = false) String password){
        return username+" "+password;
    }

    //上传文件
    @RequestMapping("/getFile")
    public void getFile(@RequestPart("file") MultipartFile file) throws IOException {
        //获取上传的文件名称
        String fileName = file.getOriginalFilename();
        //获取文件后缀名 .xxx
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        //创建唯一标识
        UUID uuid = UUID.randomUUID();
        fileName = uuid+suffix;
        file.transferTo(new File("D:\\photo\\"+fileName));
    }

    //获取Request和Response
//    public void getReqResp(HttpServletRequest req, HttpServletResponse resp){
//
//    }

    //获取cookie
//    @RequestMapping("/getCookie")
//    public Object getCookie(HttpServletRequest req,HttpServletResponse resp){
//        Cookie[] cookies = req.getCookies();
//        return cookies;
//    }

//    @RequestMapping("/getCookie")
//    public Object getCookie(@CookieValue(value = "user",required = false) String cookie){
//        return 0;
//    }

    //获取req,resp
//    @RequestMapping("/getReqResp")
//    public void getReqResp(HttpServletRequest req,HttpServletResponse resp){
//        Cookie[] cookies = req.getCookies();
//        String userAgent = req.getHeader("User-Agent");
//    }
//    @RequestMapping("/header")
//    public String getHeader(@RequestHeader(value = "User-Agent",required = false) String userAgent){
//        return userAgent;
//    }

    //存储Session
    @RequestMapping("/setSession")
    public String setSession(HttpServletRequest req){
        HttpSession session = req.getSession(true);//没有就创建session
        session.setAttribute("user","张三");
        return "set success!";
    }

    //获取Session
    @RequestMapping("/getSession")
    public String getSession(@SessionAttribute(value = "user",required = false) String username){
       return username;
    }

    @RequestMapping("/test")
    @ResponseBody
    public Object test(){
        Map<String,Object> m = new HashMap<>();
        m.put("username","张三");
        m.put("age",18);
        return m;
    }

    @RequestMapping("/forward")
    public String forward(){
        return "forward:/index.html";
    }

    @RequestMapping("/redirect")
    public String redirect(){
        return "redirect:/index.html";
    }
}
