package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    //JDBC事务管理器
    @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;

    //事务属性
    @Autowired
    private TransactionDefinition transactionDefinition;

    @RequestMapping("/delete")
    public int delete(Integer id){
        if(id==null && id<0) return 0;
        TransactionStatus transactionStatus = null;
        try{
            //开启事务
            transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
            //int n = userService.delete(id);
            //提交事务
            dataSourceTransactionManager.commit(transactionStatus);
            return 0;
        }catch (Exception e){
            //回滚事务
            dataSourceTransactionManager.rollback(transactionStatus);
        }
        return 0;
    }
}
