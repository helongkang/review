package com.example.demo.controller;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController2 {


    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping("/delete")
    public int delete(Integer id){
        if(id==null || id<0){
            return 0;
        }
        try{
            int n = 0;
            //n = userService.delete(id);
            return n;
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return 0;
    }
}
