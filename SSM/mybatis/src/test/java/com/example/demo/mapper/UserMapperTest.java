package com.example.demo.mapper;

import com.example.demo.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest //表示当前类是运行在spring boot环境中(不可省略)
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;
    @Test
    void getUserById() {
        User user = userMapper.getUserById(1);
        System.out.println(user);
    }

    @Test
    void getUserByUsername() {
        User user = userMapper.getUserByUsername("admin");
        System.out.println(user);
    }

    @Test
    void getUser() {
        User user = new User();
        user.setUsername("admin"); //用户名正确
        user.setPassword(" ' or 1='1"); //密码错误
        User get = userMapper.getUser(user);
        System.out.println(get);
    }

    @Test
    void insertOne() {
        User user = new User();
        user.setUsername("abc");
        user.setPassword("123");
        int n = userMapper.insertOne(user);
        System.out.println(n);
    }

    @Test
    void insertGetId() {
        User user = new User();
        user.setUsername("abcde");
        user.setPassword("12345");
        int n = userMapper.insertGetId(user);
        System.out.println(user.getId());
    }

    @Test
    void deleteById() {
        int n = userMapper.deleteById(5);
        System.out.println(n);
    }

    @Test
    void updateById() {
        int n = userMapper.updateById(1,"12345");
        System.out.println(n);
    }

    @Test
    void getByPartName() {
        String partName = "a";
        List<User> users = userMapper.getByPartName(partName);
        System.out.println(users);
    }

    @Test
    void insertUser() {
        User user = new User();
        user.setUsername("李四");
        user.setPassword("12345");
        user.setPhoto("李四.jpg");
        int n = userMapper.insertUser(user);
        System.out.println(n);
    }

    @Test
    void deleteByIds() {
        int[] ids = {6,7,8,9};
        int n = userMapper.deleteByIds(ids);
        System.out.println(n);
    }
}