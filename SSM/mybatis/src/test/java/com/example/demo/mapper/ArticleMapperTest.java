package com.example.demo.mapper;

import com.example.demo.model.vo.ArticleVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ArticleMapperTest {

    @Autowired
    private ArticleMapper articleMapper;
    @Test
    void getArticle() {
        ArticleVO articleVO = articleMapper.getArticle(1);
        System.out.println(articleVO);
    }
}