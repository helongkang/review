package com.example.demo.mapper;

import com.example.demo.model.vo.ArticleVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ArticleMapper {
    ArticleVO getArticle(@Param("id") Integer id); //查询某篇文章的信息及用户username
}
