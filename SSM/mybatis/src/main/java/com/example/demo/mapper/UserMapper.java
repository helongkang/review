package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper //托管给容器
public interface UserMapper {
    List<User> getUsers();

    User getUserById(@Param("id") Integer id);

    User getUserByUsername(@Param("username") String usm);

    User getUser(User user);

    int insertOne(User user);

    int insertGetId(User user); //添加返回主键

    int deleteById(@Param("id") Integer id);

    //根据id修改password
    int updateById(@Param("id") Integer id,@Param("password") String password);

    //根据名称模糊查询
    List<User> getByPartName(@Param("partName") String partName);

    int insertUser(User user);

    int deleteByIds(int[] ids);
}
