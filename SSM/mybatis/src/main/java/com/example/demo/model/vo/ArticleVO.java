package com.example.demo.model.vo;

import com.example.demo.model.Article;
import lombok.Data;


@Data
public class ArticleVO extends Article {
    private String username;

    @Override
    public String toString() {
        return "ArticleVO{" +
                "username='" + username + '\'' +
                '}'+super.toString();
    }
}
