

import com.beans.controller.CatController;
import com.beans.controller.StudentController;
import com.beans.controller.UserController;
import com.model.Cat;
import com.model.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        //Student student = (Student)context.getBean("s");
//        Student student = context.getBean("s",Student.class);
//        student.sayHi();
        //使用注解存bean
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config2.xml");
        //bean的id为类名小驼峰的表示方式
//        UserController userController = context.getBean("userController",UserController.class);
//        userController.sayHi();

        //使用@Bean注解
//        User user = context.getBean("user1",User.class);
//        System.out.println(user.getUsername());

        //依赖注入
//        UserController userController = context.getBean("userController", UserController.class);
//        userController.sayHi();

        CatController controller = context.getBean("catController", CatController.class);
        controller.getCat();
    }
}
