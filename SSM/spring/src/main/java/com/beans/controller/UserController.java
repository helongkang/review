package com.beans.controller;

import com.beans.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    @Autowired
    public UserService userService;

    public void sayHi(){
        userService.sayHi();
    }

}
