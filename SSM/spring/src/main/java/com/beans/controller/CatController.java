package com.beans.controller;

import com.model.Cat;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class CatController {


    @Resource(name = "cat2")
    public Cat cat;



    public void getCat(){
        System.out.println(cat);
    }
}
