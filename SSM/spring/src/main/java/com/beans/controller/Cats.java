package com.beans.controller;

import com.model.Cat;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class Cats {
    @Bean
    public Cat cat1(){
        Cat cat1 = new Cat();
        cat1.setName("花猫");
        return cat1;
    }

    @Bean
    public Cat cat2(){
        Cat cat2 = new Cat();
        cat2.setName("黑猫");
        return cat2;
    }
}
