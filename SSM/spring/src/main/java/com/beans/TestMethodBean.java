package com.beans;

import com.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class TestMethodBean {

    @Bean(name = {"user1","user2","user3"})
    public User getUser(){
        User user = new User();
        user.setUsername("张三");
        user.setAge(10);
        return user;
    }
}
