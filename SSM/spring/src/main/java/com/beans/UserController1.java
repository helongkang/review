package com.beans;

import com.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class UserController1 {
    public void sayHi(){
        System.out.println("do controller success");
    }

    @Bean
    public User getUser(){
        return new User();
    }
}
