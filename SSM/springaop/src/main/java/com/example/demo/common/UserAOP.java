package com.example.demo.common;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

//@Component //交给容器管理
//@Aspect //切面
//public class UserAOP {
//
//    //定义切点，设置拦截规则
//    @Pointcut("execution(* com.example.demo.controller.UserController.* (..))")
//    public void pointcut(){}
//
//    //定义通知
//    @Before("pointcut()")
//    public void doBefore(){
//        System.out.println("执行了前置通知："+ LocalDateTime.now());
//    }
//
//    @After("pointcut()")
//    public void doAfter(){
//        System.out.println("执行了后置通知："+LocalDateTime.now());
//    }
//
//    @Around("pointcut()")
//    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
//        System.out.println("环绕通知前");
//        Object obj = joinPoint.proceed();
//        System.out.println("环绕通知后");
//        return obj;
//    }
//}
