package com.example.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

//    @RequestMapping("/hello")
//    public String hello(){
//        System.out.println("hello spring aop");
//        return "hello spring aop";
//    }

    @RequestMapping("/login")
    public String login(){
        return "请登录";
    }

    @RequestMapping("/index")
    public String index(){
        return "正文";
    }

    @RequestMapping("/test")
    public String test(){
        return "hello";
    }
}
