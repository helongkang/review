package com.example.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Component
public class LoginInterceptor implements HandlerInterceptor {
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        if(session!=null && session.getAttribute("user")!=null){
            return true;
        }
        //response.setStatus(403);
        response.setContentType("application/json; charset=utf8");
        Map<String,Object> result = new HashMap<>();
        result.put("code",-1);
        result.put("msg","未登录不允许访问");
        result.put("data",null);
        response.getWriter().write(objectMapper.writeValueAsString(result));
        return false;
    }
}
