package com.example.demo.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;


//统一异常处理
@RestControllerAdvice
public class MyException {

    @ExceptionHandler(Exception.class)
    public Object handle(Exception e){
        Map<String,Object> m = new HashMap<>();
        m.put("state",-1);
        m.put("msg",e.getMessage());
        m.put("data",null);
        return m;
    }
}
