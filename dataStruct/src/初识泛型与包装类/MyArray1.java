package 初识泛型与包装类;

public class MyArray1 {
    int[] array;
    int size;  //数组中有效元素的个数

    public MyArray1(int initCapacity){
        if(initCapacity <= 0){
            initCapacity = 10;
        }
        array = new int[initCapacity];
    }

    //尾插
    public void add(int e){
        if(size >= array.length){
            System.out.println("MyArray已经存满了");
            return;
        }
        array[size] = e;
        size++;
    }

    //获取index位置上的元素
    public int get(int index){
        if(index<0 || index>=size){
            throw new ArrayIndexOutOfBoundsException("数组下标越界");
        }
        return array[index];
    }

    public static void main(String[] args) {
        MyArray1 myArray = new MyArray1(5);
        myArray.add(1);
        myArray.add(2);
    }
}
