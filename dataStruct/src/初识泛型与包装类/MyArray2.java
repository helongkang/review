package 初识泛型与包装类;

class Person1{
    int age;
    String name;
}
class Student1{
    String name;
    int age;
}
public class MyArray2 {
    Object[] array;
    int size;

    public MyArray2(int initCapacity){
        if(initCapacity <= 0){
            initCapacity = 10;
        }
        array = new Object[initCapacity];
    }

    public void add(Object e){
        if(size >= array.length){
            System.out.println("MyArray已经存满了");
            return;
        }
        array[size] = e;
        size++;
    }

    public Object get(int index){
        if(index<0 || index>=size){
            throw new ArrayIndexOutOfBoundsException("数组下标越界");
        }
        return array[index];
    }

    public static void main(String[] args) {
//        MyArray2 myArray = new MyArray2(10);
//        myArray.add(new Person());
//        myArray.add(new Person());
//        myArray.add(new Student());
//        myArray.add(new Student());
//
//        Person p1 = (Person) myArray.get(0);
//
//
//        Student s1 = (Student)myArray.get(0);


    }
}
