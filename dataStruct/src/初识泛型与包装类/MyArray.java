package 初识泛型与包装类;

class Person{
    int age;
    String name;
}
class Student{
    String name;
    int age;
}
public class MyArray<T> {
    T[] array;
    int size;

    public MyArray(int initCapacity){
        if(initCapacity <= 0){
            initCapacity = 10;
        }
        array = (T[])new Object[initCapacity];
    }

    public void add(T e){
        if(size >= array.length){
            System.out.println("MyArray已经存满了");
            return;
        }
        array[size] = e;
        size++;
    }

    public T get(int index){
        if(index<0 || index>=size){
            throw new ArrayIndexOutOfBoundsException("数组下标越界");
        }
        return array[index];
    }

    public static void main(String[] args) {
        MyArray<Student> myArray1 = new MyArray<>(10);
        myArray1.add(new Student());
        MyArray<Person> myArray2 = new MyArray<>(10);
        myArray2.add(new Person());

    }
}
