package homework;


class Student{
    private int sNo;
    private String sName;
    private String sSex;
    private int sAge;
    private int sJava;
    public Student(int sNo, String sName, String sSex, int sAge, int sJava) {
        this.sNo = sNo;
        this.sName = sName;
        this.sSex = sSex;
        this.sAge = sAge;
        this.sJava = sJava;
    }
    public int getsNo() {
        return sNo;
    }
    public void setsNo(int sNo) {
        this.sNo = sNo;
    }
    public String getsName() {
        return sName;
    }
    public void setsName(String sName) {
        this.sName = sName;
    }
    public String getsSex() {
        return sSex;
    }
    public void setsSex(String sSex) {
        this.sSex = sSex;
    }
    public int getsAge() {
        return sAge;
    }
    public void setsAge(int sAge) {
        this.sAge = sAge;
    }
    public int getsJava() {
        return sJava;
    }
    public void setsJava(int sJava) {
        this.sJava = sJava;
    }

}
public class Test4 {

    public static void main(String[] args) {
        // 学员信息
        Student stu1 = new Student(1, "孙悟空", "男", 500, 90);
        Student stu2 = new Student(2, "猪八戒", "男", 123, 91);
        Student stu3 = new Student(3, "沙和尚", "男", 243, 92);
        Student stu4 = new Student(4, "牛魔王", "男", 360, 93);
        Student stu5 = new Student(5, "哪吒", "女", 78, 94);
        Student [] stus = new Student[5];
        stus[0] = stu1;
        stus[1] = stu2;
        stus[2] = stu3;
        stus[3] = stu4;
        stus[4] = stu5;
        System.out.println("---信息---");
        for(int i = 0; i<stus.length;i++){
            System.out.println(stus[i].getsNo()+"\t"
                    +stus[i].getsName()
                    +"\t"+stus[i].getsSex()
                    +"\t"+stus[i].getsAge()
                    +"\t"+stus[i].getsJava());
        }

        int sum = 0;
        for(int i = 0; i<stus.length;i++){
            sum += stus[i].getsJava();
        }
        System.out.println("平均成绩："+(sum/5));
    }
}

