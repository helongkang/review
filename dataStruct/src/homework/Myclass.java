package homework;

public class Myclass {
    //实现两个字符串数组的逆序排序，输出结果为字符串数组
    public static String[] reverse(String[] str1, String[] str2) {
        String[] ret = new String[str1.length+str2.length];
        int index = 0;
        for(int i = str1.length-1; i >= 0; i--) {
            ret[index++] = str1[i];
        }
        for (int i = str2.length-1; i >= 0 ; i--) {
            ret[index++] = str2[i];
        }
        return ret;
    }
    //求两个整形数组的交集
    public static void intersect(int[] arr1, int[] arr2) {
        int flag = 0;
        for(int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if(arr1[i] == arr2[j]) {
                    flag = 1;
                    System.out.print(arr1[i] + " ");
                }
            }
        }
        if(flag == 0) {
            System.out.println("两个整形数组的交集为空");
        }
    }
    //求两个浮点型数组的并集
    public static void union(double[] arr1, double[] arr2) {
        double[] ret = new double[arr1.length+arr2.length];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = Integer.MAX_VALUE;
        }
        int index = 0;
        for (int i = 0; i < arr1.length; i++) {
            ret[index++] = arr1[i];
        }
        for (int i = 0; i < arr2.length; i++) {
            int flag = 0;
            for (int j = 0; ret[j] < Integer.MAX_VALUE; j++) {
                if(arr2[i] == ret[j]) {
                    flag = 1;
                }
            }
            if(flag == 0) {
                ret[index++] = arr2[i];
            }
        }
        for (int i = 0; ret[i] < Integer.MAX_VALUE; i++) {
            System.out.print(ret[i] + " ");
        }
    }
}

