package homework;

public class Computer {
    private String computerName;
    private double computerPrice;
    private String computerType;

    //无参的构造方法
    public Computer(){}

    //有参的构造方法
    public Computer(String computerName, double computerPrice, String computerType) {
        this.computerName = computerName;
        this.computerPrice = computerPrice;
        this.computerType = computerType;
    }

    //方法1: 玩游戏
    public void play() {
        System.out.println("使用" + this.computerName + "电脑学习");
    }
    //方法2: 打印电脑的属性
    public void printComputer() {
        System.out.println("============打印电脑属性============");
        System.out.println("电脑品牌为: " + this.computerName);
        System.out.println("电脑类型为: " + this.computerType);
        System.out.println("电脑价格为: " + this.computerPrice);
        System.out.println("=================================");
    }
    //方法3: 追剧
    public void watch() {
        System.out.println("使用" + this.computerName + "电脑看电影");
    }

        public static void main(String[] args) {
            Computer computer = new Computer("联想",5500,"台式");
            computer.printComputer();
            computer.play();
            computer.watch();
        }
}

