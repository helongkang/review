package homework;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Work1 {
    public static void getAge(String s){
        String ret = s.substring(6,10);
        int result = Integer.valueOf(ret);
        System.out.println(2022-result);
    }

    public static void getRandom(){
        List<Integer> list = new ArrayList<>();
        Random ran = new Random();
        for(int i = 0;i < 5;i++){
            int temp = ran.nextInt(100);
            list.add(temp);
        }

        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()){
            System.out.print(iterator.next()+" ");
        }
        System.out.println();
    }

    public static void tongJi(String s){
        int count1 = 0; //大写
        int count2 = 0; //小写
        int count3 = 0; //数字
        int count4 = 0; //其他
        for(int i = 0;i < s.length();i++){
            char c = s.charAt(i);
            if(c>='0' && c<='9'){
                count3++;
            }else if(c>='a' && c<='z'){
                count2++;
            }else if(c>='A' && c<='Z'){
                count1++;
            }else {
                count4++;
            }
        }
        System.out.println("大写字母个数："+count1);
        System.out.println("小写字母个数："+count2);
        System.out.println("数字个数："+count3);
        System.out.println("其他字符个数个数："+count4);
    }

    public static String reverse(String s){
        StringBuilder sb = new StringBuilder(s);
        sb.reverse();
        return sb.toString();
    }

    public static String zhuanHuan(String s){
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i < s.length();i++){
            char c = s.charAt(i);
            if(c>='A' && c<='Z'){
                sb.append(s.substring(i,i+1).toLowerCase());
            }else if(c>='a' && c<='z'){
                sb.append( s.substring(i,i+1).toUpperCase());
            }
        }
        return sb.toString();
    }
    public static void main(String[] args) {
        String s = "610122200210132351";
        getAge(s);
        getRandom();
        String s1 = "adafa3131fa*-+ANXxeia123";
        tongJi(s1);
        String s2 = "abcDEF";
        System.out.println(reverse(zhuanHuan(s2)));
    }
}
