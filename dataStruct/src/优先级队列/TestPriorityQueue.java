package 优先级队列;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class TestPriorityQueue {
    public static void main1(String[] args) {
        PriorityQueue<Integer> p1 = new PriorityQueue<>(); //容量默认为11
        PriorityQueue<Integer> p2 = new PriorityQueue<>(10); //参数为初始容量
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        PriorityQueue<Integer> p3 = new PriorityQueue<>(list); //使用集合list作为参数构造优先级队列
    }

    public static void main(String[] args) {
        PriorityQueue<Integer> p = new PriorityQueue<>();
        p.offer(1);
        p.offer(2);
        p.offer(3);
        System.out.println(p.size());
        System.out.println(p.isEmpty());
        p.clear();
        System.out.println(p.isEmpty());
//        System.out.println(p.peek());
//        System.out.println(p.poll());
//        System.out.println(p.size());
//        p.clear();
//        System.out.println(p.peek());
//        System.out.println(p.poll());
        //System.out.println(p.size());
        //p.offer(null);
    }
}
