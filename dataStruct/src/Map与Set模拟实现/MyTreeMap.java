package Map与Set模拟实现;

public class MyTreeMap <K extends Comparable<K>,V> implements MyMap<K,V> {
    public static class TreeNode<K,V>{
        TreeNode left;
        TreeNode right;
        MyMap.Entry<K,V> kv;

        public TreeNode(K key,V value){
            kv = new Entry<>();
            kv.key = key;
            kv.value = value;
        }
    }

    TreeNode<K,V> root = null;
    int size = 0;
    @Override
    public V put(K key, V value) {
        if(root == null){
            root = new TreeNode<>(key,value);
            size++;
            return value;
        }
        TreeNode<K,V> cur = root;
        TreeNode<K,V> parent = null;
        while(cur != null){
            parent = cur;
            int ret = key.compareTo(cur.kv.key);
            if(ret > 0){
                cur = cur.right;
            }else if(ret < 0){
                cur = cur.left;
            }else {
                V oldValue = cur.kv.value;
                cur.kv.value = value;
                return oldValue;
            }
        }
        int ret = key.compareTo(parent.kv.key);
        TreeNode<K,V> newNode = new TreeNode<>(key,value);
        if(ret > 0){
            parent.right = newNode;
        }else {
            parent.left = newNode;
        }
        size++;
        return value;
    }

    @Override
    public V get(K key) {
        TreeNode<K,V> cur = root;
        while(cur != null){
            int ret = key.compareTo(cur.kv.key);
            if(ret == 0){
                return cur.kv.value;
            }else if(ret > 0){
                cur = cur.right;
            }else {
                cur = cur.left;
            }
        }
        return null;
    }

    @Override
    public V getOrDefault(K key, V defaultValue) {
        V value = get(key);
        if(value == null){
            return defaultValue;
        }else {
            return value;
        }
    }

    @Override
    public V remove(K key) {
        TreeNode<K,V> cur = root;
        TreeNode<K,V> parent = null;
        while(cur != null){
            int ret = key.compareTo(cur.kv.key);
            if(ret > 0){
                parent = cur;
                cur = cur.right;
            }else if(ret < 0){
                parent = cur;
                cur = cur.left;
            }else {
                break;
            }
        }
        if(cur == null){
            return null;
        }
        V value = cur.kv.value;
        if(cur.left == null){
            //cur可能是叶子结点，cur也可能只有右孩子
            if(parent == null){ //cur是根节点
                root = cur.right;
            }else {
                if(cur == parent.left){
                    parent.left = cur.right;
                }else {
                    parent.right = cur.right;
                }
            }
            cur.right = null;
        }else if(cur.right == null){
            //cur可能是叶子结点，cur也可能只有左孩子
            if(parent == null){
                root = cur.left;
            }else {
                if(cur == parent.left){
                    parent.left = cur.left;
                }else {
                    parent.right = cur.left;
                }
            }
            cur.left = null;
        }else {
            //cur的左右孩子均存在
            //在右子树中找最小的结点
            TreeNode<K,V> delNode = cur.right;
            parent = cur;
            while(delNode.left != null){
                parent = delNode;
                delNode = delNode.left;
            }
            //将替代结点中的值域交给待删除结点cur
            cur.kv = delNode.kv;
            //删除
            if(parent.left == delNode){
                parent.left = delNode.right;
            }else {
                parent.right = delNode.right;
            }
        }
        return value;
    }

    @Override
    public boolean containsKey(K key) {
        TreeNode<K,V> cur = root;
        while(cur != null){
            int ret = key.compareTo(cur.kv.key);
            if(ret == 0){
                return true;
            }else if(ret > 0){
                cur = cur.right;
            }else {
                cur = cur.left;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(V value) {
        return inOrder(root,value);
    }
    private boolean inOrder(TreeNode<K,V> _root,V value){
        if(_root == null){
            return false;
        }
        if(inOrder(_root.left,value)){
            return true;
        }
        if(value.equals(_root.kv.value)){
            return true;
        }
        return inOrder(_root.right,value);
    }

    @Override
    public int size() {
        return size;
    }
}
