package 线性表List;

import java.util.Arrays;

public class MyArrayList<E>{
    private E[] elementData;
    private int size;

    private static final int DEFAULT_CAPACITY = 10;

    public MyArrayList(){
        this(DEFAULT_CAPACITY);
    }

    public MyArrayList(int initCapacity){
        if(initCapacity <= 0){
            initCapacity = DEFAULT_CAPACITY;
        }
        elementData = (E[]) new Object[initCapacity];
    }

    public boolean add(E e){
        add(size,e);
        return true;
    }

    public void ensureCapacity(int size){
        int oldCapacity = elementData.length;
        if(size >= oldCapacity){
            int newCapacity = oldCapacity + (oldCapacity>>1);
            elementData = Arrays.copyOf(elementData,newCapacity);
        }
    }

    public void add(int index,E e){
        if(index < 0 || index > size){
            throw new ArrayIndexOutOfBoundsException("add：index越界");
        }
        ensureCapacity(size);
        for(int i = size-1;i >= index;i--){
            elementData[i+1] = elementData[i];
        }
        elementData[index] = e;
        size++;
    }

    public E remove(int index){
        if(index < 0 || index >= size){
            throw new ArrayIndexOutOfBoundsException("remove：index越界");
        }
        E ret = elementData[index];
        for(int i = index+1;i < size;i++){
            elementData[i-1] = elementData[i];
        }
        size--;
        return ret;
    }

    public boolean remove(E e){
        int index = indexOf(e);
        if(index == -1){
            return false;
        }
        remove(index);
        return true;
    }

    public int indexOf(E e){
        for(int i = 0;i < size;i++){
            if(e.equals(elementData[i])){
                return i;
            }
        }
        return -1;
    }

    public int lastIndexOf(E e){
        for(int i = size-1;i >= 0;i--){
            if(e.equals(elementData[i])){
                return i;
            }
        }
        return -1;
    }

    public boolean contains(E e){
        return -1==indexOf(e);
    }

    MyArrayList<E> subList(int from,int to){
        if(from > to){
            throw new IllegalArgumentException("subList：非法参数，from>to");
        }
        int newSize = to-from;
        MyArrayList<E> list = new MyArrayList<>(newSize);
        while(from < to){
            list.add(elementData[from]);
            from++;
        }
        return list;
    }

    public int size(){
        return size;
    }

    public E get(int index){
        if(index < 0 || index >= size){
            throw new ArrayIndexOutOfBoundsException("get：index越界");
        }
        return elementData[index];
    }

    public void set(int index,E e){
        if(index < 0 || index >= size){
            throw new ArrayIndexOutOfBoundsException("set：index越界");
        }
        elementData[index] = e;
    }

    public void clear(){
        size = 0;
        for(int i  =0;i < size;i++){
            elementData[i] = null;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for(int i = 0;i < size-1;i++){
            sb.append(elementData[i]);
            sb.append(",");
        }
        sb.append(elementData[size-1]);
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
        MyArrayList<Integer> list = new MyArrayList<>(4);
        list.add(1);   //尾插1
        list.add(2);   //尾插2
        list.add(3);   //尾插3
        list.add(3,4);  //在位置3插入4
        System.out.println(list);
        System.out.println(list.get(2));  //输出下标为2的元素
        list.set(0,5); //将位置0的元素设置为5
        System.out.println(list);
        System.out.println(list.contains(4)); //判断是否包含4
        System.out.println(list.contains(9)); //判断是否包含9
        list.remove(0); //删除位置0的元素
        System.out.println(list);
        list.remove((Integer) 4); //删除元素4
        System.out.println(list);
        list.add(3);
        System.out.println(list.indexOf(3)); //输出第一个3的位置
        System.out.println(list.lastIndexOf(3)); //输出最后一个3的位置
        MyArrayList<Integer> list2 = list.subList(0,1); //截取部分List
        System.out.println(list2);
        list.clear(); //清空
        System.out.println(list.size()); //看看是否清空
    }
}
