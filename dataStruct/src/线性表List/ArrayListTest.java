package 线性表List;

import java.util.ArrayList;
import java.util.List;

public class ArrayListTest {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(1,9);
        List<Integer> list1 = new ArrayList<>(); //创建新线性表list1
        list1.add(6);
        list1.add(8);
        list.addAll(list1);  //将list1尾插到list中
        list.remove(0);
        Integer a = 3;
        list.remove(a);
        int b = list.get(2);
        System.out.println(b);
        list.set(2,7);
        boolean flag1 = list.contains(2);
        boolean flag2 = list.contains(5);
        System.out.println(flag1);
        System.out.println(flag2);
        list.add(2);
        System.out.println(list.indexOf(2));
        System.out.println(list.lastIndexOf(2));
        list.clear();
        System.out.println(list.size());
        list.add(1);
        list.add(2);
        list.add(3);
        for(int i : list){
            System.out.print(i+" ");
        }
    }
}
