package 线性表List;

public class MyLinkedList<E> {
    private static class ListNode<E> {
        ListNode<E> pre;  //指向前一个结点
        ListNode<E> next; //指向下一个结点
        E val;  //结点的值

        public ListNode(E val){
            this.val = val;
        }
    }

    private ListNode<E> first;  //指向链表首结点
    private ListNode<E> last;  //指向链表尾结点
    private int size;  //链表结点的个数

    public MyLinkedList() {

    }

    public void addFirst(E e){
        ListNode<E> newNode = new ListNode<>(e);
        if(first == null){
            first = newNode;
            last = newNode;
        }else {
            first.pre = newNode;
            newNode.next = first;
            first = newNode;
        }
        size++;
    }

    public void addLast(E e){
        ListNode<E> newNode = new ListNode<>(e);
        if(first == null){
            first = newNode;
            last = newNode;
        }else {
            last.next = newNode;
            newNode.pre = last;
            last = newNode;
        }
        size++;
    }

    public boolean addIndex(int index,E e){
        ListNode<E> newNode = new ListNode<>(e);
        if(index < 0 && index > size){
            throw new IndexOutOfBoundsException("addIndex：下标越界");
        }
        if(index == size){
            addLast(e);
        }else if(index == 0){
            addFirst(e);
        }else {
            ListNode<E> cur = first;
            for(int i = 0;i < index;i++){
                cur = cur.next;
            }
            newNode.pre = cur.pre;
            newNode.next = cur;
            newNode.pre.next = newNode;
            cur.pre = newNode;
            size++;
        }
        return true;
    }

    public void remove(E e){
        ListNode<E> cur = first;
        while(cur != null){
            if(e.equals(cur.val)){
                break;
            }
            cur = cur.next;
        }
        if(cur == first){
            first = first.next;
            first.pre = null;
        }else if(cur == last){
            last = last.pre;
            last.next = null;
        }else {
            cur.pre.next = cur.next;
            cur.next.pre = cur.pre;
        }
        size--;
    }

    public void removeAll(E e){
        ListNode<E> cur = first;
        while(cur != null){
            if(cur.val.equals(e)){
                if(cur == first){
                    first = first.next;
                    first.pre = null;
                }else if(cur == last){
                    last = last.pre;
                    last.next = null;
                }else {
                    cur.pre.next = cur.next;
                    cur.next.pre = cur.pre;
                }
                size--;
            }
            cur = cur.next;
        }
    }

    public void clear(){
        first = null;
        last = null;
        size = 0;
    }

    public boolean isEmpty(){
        //return size==0;
        return first==null;
    }

    public int size(){
        return size;
    }

    public int indexOf(E e){
        ListNode<E> cur = first;
        int index = 0;
        while(cur != null){
            if(e.equals(cur.val)){
                return index;
            }else {
                index++;
                cur = cur.next;
            }
        }
        return -1;
    }

    public int lastIndexOf(E e){
        ListNode<E> cur = last;
        int index = size-1;
        while(cur != null){
            if(e.equals(cur.val)){
                return index;
            }else {
                cur = cur.pre;
                index--;
            }
        }
        return -1;
    }

    public boolean contains(E e){
        return -1 != indexOf(e);
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if(first == null){
            sb.append("]");
        }else {
            ListNode<E> cur = first;
            while(cur.next != null){
                sb.append(cur.val);
                sb.append(",");
                cur = cur.next;
            }
            sb.append(cur.val);
            sb.append("]");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        MyLinkedList<Integer> list = new MyLinkedList<>();
//        list.addFirst(1);
//        list.addFirst(2);
//        list.addFirst(3);
//        list.addFirst(4);
        list.addLast(1);
        list.addLast(2);
        list.addLast(3);
        list.addLast(4);
        list.addLast(5);
//        list.addIndex(0,1);
//        list.addIndex(5,5);
//        list.addIndex(3,3);
        list.remove(1);
        list.remove(5);
        list.remove(3);
        list.clear();
        list.addFirst(1);
//        boolean ret1 = list.isEmpty();
//        System.out.println(ret1);
//        list.clear();
//        boolean ret2 = list.isEmpty();
//        System.out.println(ret2);
//        System.out.println(list);
//        list.clear();
//        System.out.println(list.size());
//        list.addFirst(1);
//        System.out.println(list.size());
//        list.clear();
//        list.addLast(1);
//        list.addLast(2);
//        list.addLast(1);
//        list.addLast(3);
//        list.addLast(1);
//        int ret = list.indexOf(2);
//        System.out.println(ret);
//        System.out.println(list);
        list.clear();
        list.addLast(1);
        list.addLast(2);
        list.addLast(3);
        list.addLast(3);
        list.addLast(4);
        list.addLast(5);
//        System.out.println(list.lastIndexOf(3));
//        System.out.println(list.lastIndexOf(6));
//        System.out.println(list.indexOf(3));
//        System.out.println(list.indexOf(6));
        boolean ret = list.contains(2);
        boolean ret1 = list.contains(6);
        System.out.println(ret);
        System.out.println(ret1);
    }
}
