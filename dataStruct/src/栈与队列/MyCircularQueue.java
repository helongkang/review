package 栈与队列;

public class MyCircularQueue<E> {
    private E[] array;
    private int front; //标记队列头，进行出队列
    private int rear; //标记队列尾，进行入队列
    private int count; //队列中元素个数，用来区分队列满与空
    private int N; //数组的长度

    public MyCircularQueue(){
        array = (E[])new Object[10];
        N = array.length;
    }

    public boolean enQueue(E val){
        if(isFull()){
            return false;
        }
        array[rear] = val;
        rear++;
        if(rear == N){
            rear = 0;
        }
        count++;
        return true;
    }
    public boolean deQueue(){
        if(isEmpty()){
            return false;
        }
        count--;
        front++;
        if(front == N){
            front = 0;
        }
        return true;
    }
    public E rear(){
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        return array[(rear-1+N)%N];
    }
    public E front(){
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        return array[front];
    }
    public boolean isEmpty(){
        return count==0;
    }
    public boolean isFull(){
        return count==N;
    }

}
