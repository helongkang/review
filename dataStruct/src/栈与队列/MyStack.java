package 栈与队列;

import java.util.Arrays;

public class MyStack<E> {
    private E[] array;
    private int size;

    public MyStack(){
        array = (E[])new Object[3];
    }

    public void push(E e){
        ensureCapacity();
        array[size] = e;
        size++;
    }

    public E pop(){
        E ret = peek();
        size--;
        return ret;
    }

    public E peek(){
        if(empty()){
            throw new RuntimeException("stack为空");
        }
        return array[size-1];
    }

    public int size(){
        return size;
    }

    public boolean empty(){
        return size==0;
    }

    private void ensureCapacity(){
        if(size==array.length){
            Arrays.copyOf(array,size*2);
        }
    }
}
