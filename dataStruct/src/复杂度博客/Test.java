package 复杂度博客;

public class Test {
    public void func1(int N){
        int count = 0;
        for(int i = 0;i < N;i++){
            for(int j = 0;j < N;j++){
                count++;
            }
        }
        for(int k = 0;k < 2*N;k++){
            count++;
        }
        int M = 10;
        while((M--) > 10){
            count++;
        }
    }
    public void fun2(int N){
        int count = 0;
        for(int k = 0;k < 2*N;k++){
            count++;
        }
        int M = 10;
        while(M > 0){
            count++;
            M--;
        }
    }

    public void fun3(int N,int M){
        int count = 0;
        for(int i = 0;i < M;i++){
            count++;
        }
        for(int j = 0;j < N;j++){
            count++;
        }
    }
    public void fun4(){
        int count = 0;
        for(int i = 0;i < 100;i++){
            count++;
        }
    }
    public void bubbleSort(int[] array){
        for(int i = 0;i < array.length-1;i++){
            for(int j = 0;j < array.length-1-i;j++){
                if(array[j] > array[j+1]){
                    int t = array[j];
                    array[j] = array[j+1];
                    array[j+1] = t;
                }
            }
        }
    }

    public int binarySearch(int[] array,int value){
        int begin = 0;
        int end = array.length-1;
        while(begin < end){
            int mid = begin+((end-begin)>>1);
            if(array[mid] < value){
                begin = mid+1;
            }else if(array[mid] > value){
                end = mid-1;
            }else {
                return mid;
            }
        }
        return -1;
    }

    public long factorial(int N){
        return N < 2 ? N : factorial(N-1)*N;
    }

//    public long factorial(int N){
//        return N < 2 ? N : factorial(N-1) + factorial(N-2);
//    }

    long[] fibonacci(int n) {
        long[] fibArray = new long[n + 1];
        fibArray[0] = 0;
        fibArray[1] = 1;
        for (int i = 2; i <= n ; i++) {
            fibArray[i] = fibArray[i - 1] + fibArray [i - 2];
        }
        return fibArray;
    }

    public int fun(){
        int[] arr = new int[10];
        arr[0] = 1;
        return arr[0];
    }
}
