package 反射枚举lambda;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class TestReflect {
    public static void main1(String[] args) {
        //获取字节码文件对象
        //1.使用Class.forName("类的全路径")
        try {
            Class<?> stuClass1 = Class.forName("反射枚举lambda.Student");
            System.out.println(stuClass1);
            //2.使用类.class
            Class<?> stuClass2 = Student.class;
            System.out.println(stuClass2);
            System.out.println(stuClass1==stuClass2); //true，字节码文件只有一份，故是同一个对象
            //3.使用对象.getClass()
            //该方法需要先创建对象，故先将Student类的构造方法改为公有的再进行下述操作
//            Student student = new Student("张三",26);
//            Class<?> stuClass3 = student.getClass();
//            System.out.println(stuClass3);
//            System.out.println(stuClass2==stuClass3); //true，字节码文件只有一份，故是同一个对象
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            //1.获取字节码对象
            Class<?> stuClass = Class.forName("反射枚举lambda.Student");
            //2.获取构造方法
            Constructor<?> stuConstructor = stuClass.getDeclaredConstructor(String.class,int.class); //参数也是class类型
            //3.修改方法的访问权限
            stuConstructor.setAccessible(true);
            //4.调用该方法
            Object object = stuConstructor.newInstance("李四",23); //newInstance()创建类的实例,为Object类型
            Student s = (Student) object;
            System.out.println(s);

            //反射属性
            Field sutAge = stuClass.getDeclaredField("age"); //参数为属性
            sutAge.setAccessible(true);
            sutAge.setInt(s,18); //设置属性值为int，第一个参数为哪个对象，第二个参数为设置值
            System.out.println(s);

            //反射方法
            Method setNameMethod = stuClass.getDeclaredMethod("setName", String.class); //第一个参数为方法名，后面参数为方法参数
            setNameMethod.setAccessible(true);
            setNameMethod.invoke(s,"王五");
            System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
