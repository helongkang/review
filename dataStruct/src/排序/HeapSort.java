package 排序;

public class HeapSort {

    public static void heapSort(int[] array){
        int size = array.length;
        int lastLeaf = (size-1-1)/2;
        for(int root = lastLeaf;root >= 0;root--){
            shiftDown(array,root,size);
        }

        int end = size-1;
        while(end > 0){
            swap(array,0,end);
            shiftDown(array,0,end);
            end--;
        }
    }


    public static void shiftDown(int[] array,int parent,int size){
        int child = parent*2+1;
        while(child < size){
            if(child+1<size && array[child+1]>array[child]){
                child = child+1;
            }
            if(array[parent] < array[child]){
                swap(array,parent,child);
                parent = child;
                child = parent*2+1;
            }else {
                return;
            }
        }
    }


    public static void swap(int[] array,int a,int b){
        int t = array[a];
        array[a] = array[b];
        array[b] = t;
    }

    public static void main(String[] args) {
        int[] array = {5,6,9,4,2,3,1,6,7,8};
        heapSort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
