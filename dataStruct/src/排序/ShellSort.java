package 排序;

public class ShellSort {
    public static void shellSort(int[] array){
        int gap = array.length;
        while(gap > 1){
            gap = gap/3+1;
            for(int i = gap;i < array.length;i++) {
                int key = array[i];
                int pos = i - gap;
                while (pos >= 0 && key < array[pos]) {
                    array[pos+gap] = array[pos];
                    pos -= gap;
                }
                array[pos+gap] = key;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {5,6,9,4,2,3,1,6,7,8};
        shellSort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
