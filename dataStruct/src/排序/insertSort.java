package 排序;

public class insertSort {

    public static void insertSort(int[] array){
        //第一个数默认有序，从第二个开始插入
        for(int i = 1;i < array.length;i++){
            int key = array[i]; //要插入的值
            int pos = i-1; //插入的位置
            while(pos>=0 && key<array[pos]){ //找空位置
                array[pos+1] = array[pos];
                pos--;
            }
            //找到了插入
            array[pos+1] = key;
        }
    }

    public static void swap(int[] array,int a,int b){
        int t = array[a];
        array[a] = array[b];
        array[b] = t;
    }
    public static void main(String[] args) {
        int[] array = {5,6,9,4,2,3,1,6,7,8};
        insertSort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
