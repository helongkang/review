package 排序;

public class QuickSort {

    public static void quickSort(int[] array){
        quickSort(array,0,array.length);
    }

    public static void quickSort(int[] array,int left,int right){
        if(right-left>1){
            int div = partition(array,left,right);
            quickSort(array,left,div);
            quickSort(array,div+1,right);
        }
    }

    public static int partition(int[] array,int left,int right){
        int key = array[left];
        int begin = left;
        int end = right-1;
        while(begin < end){
            while(begin<end && array[end]>=key){
                end--;
            }
            array[begin] = array[end];
            while(begin<end && array[begin]<=key){
                begin++;
            }
            array[end] = array[begin];
        }
        array[begin] = key;
        return begin;
    }
    public static int partition1(int[] array,int left,int right){
        int key = array[right-1];
        int begin = left;
        int end = right-1;
        while(begin < end){
            while(begin<end && array[begin]<=key){
                begin++;
            }
            while(begin<end && array[end]>=key){
                end--;
            }
            swap(array,begin,end);
        }
        swap(array,begin,right-1);
        return begin;
    }
    public static void swap(int[] array,int a,int b){
        int t = array[a];
        array[a] = array[b];
        array[b] = t;
    }

    public static void main(String[] args) {
        int[] array = {5,6,9,4,2,3,1,6,7,8};
        quickSort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
