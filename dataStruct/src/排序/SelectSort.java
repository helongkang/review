package 排序;

public class SelectSort {
    public static void selectSort(int[] array){
        //选择排序：每次选最小的放在最前面
        int size = array.length;
        for(int i = 0;i < size-1;i++){
            int pos = i;
            for(int j = i+1;j < size;j++){
                if(array[j] < array[pos]){
                    pos = j;
                }
            }
            swap(array,i,pos);
        }
    }

    public static void swap(int[] array,int a,int b){
        int t = array[a];
        array[a] = array[b];
        array[b] = t;
    }
    public static void main(String[] args) {
        int[] array = {5,6,9,4,2,3,1,6,7,8};
        selectSort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
