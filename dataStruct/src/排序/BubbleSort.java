package 排序;

public class BubbleSort {


    public static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            boolean flag = false;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j + 1] < array[j]) {
                    int t = array[j+1];
                    array[j+1] = array[j];
                    array[j] = t;
                    flag = true;
                }
            }
            if(flag == false){
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {5,6,9,4,2,3,1,6,7,8};
        bubbleSort(array);
        for(int i : array){
            System.out.print(i+" ");
        }
    }
}
