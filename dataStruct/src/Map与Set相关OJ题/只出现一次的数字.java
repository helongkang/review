package Map与Set相关OJ题;

import java.util.HashSet;
import java.util.Set;

public class 只出现一次的数字 {
    public int singleNumber(int[] nums) {
        Set<Integer> s = new HashSet<>();
        for(int i = 0;i < nums.length;i++){
            if(!s.add(nums[i])){
                s.remove(nums[i]);
            }
        }
        Object[] array = s.toArray();
        return (int)array[0];
    }
}
