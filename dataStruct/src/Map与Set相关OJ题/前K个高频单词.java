package Map与Set相关OJ题;


import java.util.*;

//比较器类
class KVCmp implements Comparator<Map.Entry<String,Integer>>{
    public int compare(Map.Entry<String,Integer> o1,Map.Entry<String,Integer> o2){
        if(o1.getValue() > o2.getValue()){
            return 1;
        }
        if(o1.getValue()==o2.getValue() && o2.getKey().compareTo(o1.getKey())>0){
            return 1;
        }
        if(o1.getValue()==o2.getValue() && o2.getKey().compareTo(o1.getKey())==0){
            return 0;
        }
        return -1;
    }
}
public class 前K个高频单词 {
    public List<String> topKFrequent(String[] words, int k) {
        Map<String,Integer> m = new HashMap<>();
        //统计次数
        for(int i = 0;i < words.length;i++){
            m.put(words[i],m.getOrDefault(words[i],0)+1);
        }
        //new比较器类
        KVCmp cmp = new KVCmp();
        //创建优先级队列，传入比较器
        PriorityQueue<Map.Entry<String,Integer>> p = new PriorityQueue<>(cmp);
        Set<Map.Entry<String,Integer>> s = m.entrySet();
        int i = 0;
        //将键值对插入到优先级队列中
        for(Map.Entry<String,Integer> kv : s){
            //前k个直接插入
            if(i < k){
                p.offer(kv);
                i++;
            }else {
                //后面的经过比较后再插入
                if(cmp.compare(kv,p.peek()) > 0){
                    p.poll();
                    p.offer(kv);
                }
            }
        }
        List<String> ret = new ArrayList<>();
        //往list中插入键值对的V，也就是单词
        for(i = 0;i < k;i++){
            ret.add(p.poll().getKey());
        }
        Collections.reverse(ret);//逆置
        return ret;
    }
}
