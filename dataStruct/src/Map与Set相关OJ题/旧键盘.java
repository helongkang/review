package Map与Set相关OJ题;

import java.util.*;
public class 旧键盘 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String s1 = sc.nextLine().toUpperCase();//应该输出的
        String s2 = sc.nextLine().toUpperCase();//实际输出的
        Set<Character> s = new HashSet<>();
        for(int i = 0;i < s2.length();i++){
            s.add(s2.charAt(i));
        }
        for(int i = 0;i < s1.length();i++){
            if(s.add(s1.charAt(i))){
                System.out.print(s1.charAt(i));
            }
        }
        System.out.println();
    }
}
