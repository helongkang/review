package 对象的比较;

import java.util.PriorityQueue;

class Person implements Comparable<Person>{
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(Person o) {
        if(o == null){
            return 1;
        }
        return this.age-o.age;
    }
}
public class Test1 {
    public static void main(String[] args) {
        Person p1 = new Person("小王",22);
        Person p2 = new Person("小张",21);
        Person p3 = new Person("小方",22);
        System.out.println(p1.compareTo(p2)); //>0表示大于
        System.out.println(p2.compareTo(p3)); //<0表示小于
        System.out.println(p1.compareTo(p3)); //==0表示相等
        PriorityQueue<Person> p = new PriorityQueue<>();
        p.offer(p1);
        p.offer(p2);
        p.offer(p3);
    }
}
