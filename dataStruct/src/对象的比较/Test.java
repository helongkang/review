package 对象的比较;

import java.util.Comparator;
import java.util.PriorityQueue;

class Student {
    String name;
    int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
class StudentComparator implements Comparator<Student>{
    @Override
    public int compare(Student o1, Student o2) {
        if(o1 == o2){
            return 0;
        }
        if(o1 == null){
            return -1;
        }
        if(o2 == null){
            return 1;
        }
        return o1.age-o2.age;
    }
}
public class Test {
    public static void main(String[] args) {
        Student s1 = new Student("张三",25);
        Student s2 = new Student("李四",31);
        Student s3 = new Student("李四",35);
        PriorityQueue<Student> p = new PriorityQueue<>(new StudentComparator());
        p.offer(s1);
        p.offer(s2);
        p.offer(s3);
    }
}
