package 鲲鹏杯正式赛;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main5 {
    public static int method(int[] array,int val){
        int left = 0;
        int right = 0;
        int sum = 0;
        int start = 0;
        int len = Integer.MAX_VALUE;
        while(right < array.length){
            if(sum < val){
                sum += array[right];
                right++;
            }
            while(sum >= val){
                start = left;
                int t = right-left;
                len = len>t?t:len;
                sum -= array[left];
                left++;
            }
        }
        return len==Integer.MAX_VALUE?0:len;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String n1 = sc.nextLine();
        int n = Integer.parseInt(n1);
        List<Integer> list = new ArrayList<>();
        for(int i = 0;i < n;i++){
           String ss = sc.nextLine();
           String[] arr = ss.split(" ");
           int a = Integer.parseInt(arr[0]);
           int b = Integer.parseInt(arr[1]);
           int[] array = new int[a];
           String sss = sc.nextLine();
           String[] arrr = sss.split(" ");
           for(int j = 0;j < a;j++){
               array[j] = Integer.parseInt(arrr[j]);
           }
           list.add(method(array,b));
        }
        for(int c : list){
            System.out.println(c);
        }
    }
}
