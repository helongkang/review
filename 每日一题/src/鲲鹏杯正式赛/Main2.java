package 鲲鹏杯正式赛;

import java.util.*;
public class Main2{
    public static String method(int[] array){
        Map<Integer,Integer> m = new HashMap<>();
        for(int i = 0;i < array.length;i++){
            m.put(i+1,array[i]);
        }
        int sum = 0;
        int part = 0;
        for(int i = 0;i < array.length;i++){
            sum += array[i]*(i+1);
        }
        if(sum%2!=0){
            return "NO";
        }else {
            int div = sum/2;
            for(int j = array.length;j>=4;j--){
                int k = j;
                while(part<div && k>=4){
                    Integer a = 0;
                    if(!m.get(j).equals(a)){
                        part += j;
                        m.put(j,m.get(j)-1);
                    }
                    k--;
                }
            }
            if(part == div){
                return "YES";
            }else {
                return "NO";
            }
        }
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        List<String> list = new ArrayList<>();
        while(sc.hasNext()){
            String s1 = sc.nextLine();
            String[] s11 = s1.split(" ");
            int[] arr = new int[s11.length];
            for(int i = 0;i < arr.length;i++){
                arr[i] = Integer.parseInt(s11[i]);
            }
            boolean flag = false;
            for(int j = 0;j < arr.length;j++){
                if(arr[j] != 0){
                    flag = true;
                }
            }
            if(flag){
                System.out.println(method(arr));
            }else {
                return;
            }

        }
    }
}
