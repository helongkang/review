package 另类加法and走方格的方案数;

import java.util.Scanner;

//走方格的方案数
//当行为1，列大于1时，方案数为m+n
//当行大于1，列等于1时，方案数为m+n
public class Main1 {
    public static int fin(int m,int n){
        if((m==1&&n>=1) || (n==1&&m>=1)){
            return m+n;
        }
        return fin(m-1,n)+fin(m,n-1);
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        System.out.println(fin(m,n));
    }
}
