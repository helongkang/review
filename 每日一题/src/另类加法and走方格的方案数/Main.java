package 另类加法and走方格的方案数;

//另类加法
public class Main {
        public int addAB(int A, int B) {
            //两个数的二进制数相异或，得到的结果是该两个数二进制相加的结果（不考虑进位）
            //两个数的二进制数想与再左移一位，得到的结果是两个二进制数相加进位后的结果（只考虑进位）
            //如果进位后的结果为0，表示不需要进位，即两数异或的结果就是两数的和
            int sum = 0;
            while(B != 0){  //判断是否发生进位
                sum = A^B;
                int carry = (A&B)<<1;
                A = sum;
                B = carry;
            }
            return sum;
        }
}
