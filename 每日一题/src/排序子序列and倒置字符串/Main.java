package 排序子序列and倒置字符串;

import java.util.*;

//排序子序列
public class Main{
    public static int method(int[] array){
        int flag = 0; //flag==1表示增,flag==-1表示减,flag==0表示新的序列刚开始
        int ret = 1;
        //相邻两个数相等时，不处理，因为此时可以是递增也可以是递减
        for(int i = 1;i < array.length;i++){
            if(array[i] > array[i-1]){
                if(flag == 0){ //如果flag==0，表示是新的序列，将flag=1表示开始递增
                    flag = 1;
                }
                if(flag == -1){ //在递增中，如果flag==-1，说明递减了一次，flag==0重新开始子序列，将ret++
                    flag = 0;
                    ret++;
                }
            }else if(array[i] < array[i-1]){
                if(flag == 0){ //如果flag==0，表示是新的序列，将flag=-1表示开始递增
                    flag = -1;
                }
                if(flag == 1){ //在递减中，如果flag==1，说明递增了一次，flag==0重新开始子序列，将ret++
                    flag = 0;
                    ret++;
                }
            }
        }
        return ret;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        int n = Integer.valueOf(s);
        int[] array = new int[n];
        for(int i = 0;i < n;i++){
            array[i] = sc.nextInt();
        }
        System.out.println(method(array));
    }
}