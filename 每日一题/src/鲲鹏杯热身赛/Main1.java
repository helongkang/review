package 鲲鹏杯热身赛;

import java.util.*;
public class Main1{
    public static int method1(int a,int b){
        if(a > b){
            int t = a;
            a = b;
            b = t;
        }
        int ret = 0;
        for(int i = 1;i <= a;i++){
            if(a%i==0 && b%i==0){
                ret = ret>i?ret:i;
            }
        }
        return ret;
    }
    public static int method2(int a,int b){
        if(a > b){
            int t = a;
            a = b;
            b = a;
        }
        int ret = 0;
        for(int i = b;i <= a*b;i++){
            if(i%a==0 && i%b==0){
                ret = i;
                break;
            }

        }
        return ret;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int a = sc.nextInt();
            int b = sc.nextInt();
            System.out.println(method1(a,b)+" "+method2(a,b));
        }
    }
}