package 最近公共祖先and求最大连续bit数;

public class Main {

    //求最近公共祖先
    public int getLCA(int a, int b) {
        if(a > b){
            int t = a;
            a = b;
            b = t;
        }
        //当a!=b的时候，谁大就将谁/2，依次循环，直到两个相等为止
        while(a != b){
            if(a==1){
                return a;
            }
            if(b/2==a){
                return a;
            }
            while(b > a){
                b = b/2;
                if(a == b){
                    return a;
                }
            }
            while(a > b){
                a = a/2;
                if(a == b){
                    return a;
                }
            }
        }
        return -1;
    }
}
