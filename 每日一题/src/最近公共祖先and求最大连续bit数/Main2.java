package 最近公共祖先and求最大连续bit数;

import java.util.Scanner;

//求最大连续bit数
//将n&1的结果，如果是1说明二进制位为1，否则为0，更新最大值，重新记录count
public class Main2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        int max = 0;
        while(n != 0){
            if((n & 1) == 1){
                count++;
                max = max>count?max:count;
            }else {
                count = 0;
            }
            n = (n>>1);
        }
        System.out.println(max);
    }
}
