package 不要二and字符串转换为整数;

//字符串转整数
public class Main1 {
    public static int StrToInt(String str) {
        if(str==null || str.length()==0){
            return 0;
        }
        if(str.length()==1 && (str.charAt(0)=='+'||str.charAt(0)=='-')){
            return 0;
        }
        boolean flag = true;
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i < str.length();i++){
            if(i == 0){
                if(str.charAt(0) == '+'){
                    continue;
                }
                if(str.charAt(0) == '-'){
                    flag = false;   //表示负数
                    continue;
                }
            }
            if(str.charAt(i)<'0' || str.charAt(i)>'9'){
                return 0;
            }
            sb.append(str.charAt(i));
        }
        String s = sb.toString();
        int ret = Integer.valueOf(s);
        if(flag == false){
            return ret*-1;
        }else {
            return ret;
        }
    }
    public static void main(String[] args) {
        String s = "+0";
        System.out.println(StrToInt(s));
    }
}
