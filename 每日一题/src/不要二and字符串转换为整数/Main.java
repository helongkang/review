package 不要二and字符串转换为整数;

import java.util.Scanner;

//不要二
public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int w = sc.nextInt();
        int h = sc.nextInt();
        boolean[][] arr = new boolean[h][w];
        int count = 0;
        for(int i = 0;i < h;i++){
            for(int j = 0;j < w;j++){
                if(arr[i][j] == false){
                    count++;
                    if(i+2<h){
                        arr[i+2][j] = true;
                    }
                    if(j+2 < w){
                        arr[i][j+2] = true;
                    }
                }
            }
        }
        System.out.println(count);
    }
}
