package 树根;

import java.util.*;
public class Main {
    //因为输入数据很大，所以使用字符串
    public static String method(String s) {
        if(s.length() == 1){
            return s;
        }
        while(s.length() > 1){
            int sum = 0;
            for(int i = 0;i < s.length();i++){
                sum += (s.charAt(i)-'0');
            }
            s = String.valueOf(sum);
        }
        return s;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String s = sc.nextLine();
            System.out.println(method(s));
        }
    }
}