package 小易的升级之路and找出字符串中第一次只出现一次的字符;

import java.util.*;
//小易的升级之路
public class Main1{
    public static int getGY(int a,int b){
        if(a > b){ //a小b大
            int t = a;
            a = b;
            b = t;
        }
        if(b % a == 0){
            return a;
        }
        int max = 0;
        for(int i = 1;i <= a;i++){
            if(a%i==0 && b%i==0){
                max = max>i?max:i;
            }
        }
        return max;
    }
    public static int getVal(int[] array,int a){
        for(int i = 0;i < array.length;i++){
            if(array[i] <= a){
                a += array[i];
            }else {
                a += getGY(a,array[i]);
            }
        }
        return a;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = sc.nextInt();
            int a = sc.nextInt();
            int[] array = new int[n];
            for(int i = 0;i < n;i++){
                array[i] = sc.nextInt();
            }
            System.out.println(getVal(array,a));
        }
    }
}
