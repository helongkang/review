package 小易的升级之路and找出字符串中第一次只出现一次的字符;

import java.util.*;
//第一次只出现一次的字符
public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        Map<Character,Integer> m = new HashMap<>();
        for(int i = 0;i < s.length();i++){
            char c = s.charAt(i);
            m.put(c,m.getOrDefault(c,0)+1);
        }
        for(int i = 0;i < s.length();i++){
            if(m.get(s.charAt(i)) == 1){
                System.out.println(s.charAt(i));
                return;
            }
        }
        System.out.println(-1);
    }
}