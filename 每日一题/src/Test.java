import java.util.*;

class Student{
    int age;
    String name;
}
public class Test{
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.reverse();
        String s = "abc";
        PriorityQueue<Student> p = new PriorityQueue<>(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.age-o2.age; //小堆，o2-o1是大堆
            }
        });
    }
}

