package 二进制中1的个数and手套;

//手套
public class Main1 {
    public static int findMinimum(int n, int[] left, int[] right) {
        int sum = 0;  //最后返回的总手套数
        int sumLeft = 0; //左手套的总数，不包含不成对的手套
        int sumRight = 0; //右手套的总数，不包含不成对的手套
        int leftMin = 26; //左手套不同颜色的最小值
        int rightMin = 26; //有手套不同颜色的最小值
        for(int i = 0;i < n;i++){
            if(left[i]==0 && right[i]!=0){
                sum+=right[i];
                continue;
            }
            if(right[i]==0 && left[i]!=0){
                sum+=left[i];
                continue;
            }
            if(left[i]==0 && right[i]==0){
                continue;
            }
            leftMin = leftMin<left[i]?leftMin:left[i];
            rightMin = rightMin<right[i]?rightMin:right[i];
            sumLeft += left[i];
            sumRight += right[i];
        }
        sumLeft = sumLeft-leftMin;
        sumRight = sumRight-rightMin;
        int x = sumLeft>sumRight?sumRight:sumLeft; //哪个手的手套数小就用哪个手的手套数算
        return sum+x+2; //手套多的一只手在其中已经拿出的基础上多拿1个，另一个1为上述注释中的1
    }
}
