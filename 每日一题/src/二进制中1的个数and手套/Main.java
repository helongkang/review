package 二进制中1的个数and手套;

import java.util.*;
//二进制中1的个数
//与1&，如果为1则count++，直到n为0
public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        while(n != 0){
            if((n&1)==1){
                count++;
            }
            n = (n>>1);
        }
        System.out.println(count);
    }
}