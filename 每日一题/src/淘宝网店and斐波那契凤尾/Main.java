package 淘宝网店and斐波那契凤尾;

import java.util.Scanner;

//淘宝网店
//月份为素数，每天赚1元，否则赚2元
public class Main {
    public static boolean isYear(int year){
        if(year%400==0 || year%4==0&&year%100!=0){
            return true;
        }
        return false;
    }
    public static int year1Count(int year,int month,int day){
        int[] array = {31,28,31,30,31,30,31,31,30,31,30,31};
        int sum = 0;
        for(int i = month;i <= 12;i++){
            if(i==2 || i==3 || i==5 || i==7 || i==11){
                sum += array[i-1];
            }else {
                sum += array[i-1]*2;
            }
        }
        if(isYear(year) && month<=2){
            sum++;
        }
        return sum;
    }
    public static int method(int year1,int month1,int day1,int year2,int month2,int day2){
        int[] array = {31,28,31,30,31,30,31,31,30,31,30,31};
        int total = 0;
        for(int i = year1+1;i < year2;i++){
            total += 579;
            if(isYear(i)){
                total++;
            }
        }
        if(year1 < year2){
            for(int i = 1;i < month2;i++){
                if(i==2 || i==3 || i==5 || i==7 || i==11){
                    total += array[i-1];
                }else {
                    total += array[i-1]*2;
                }
            }
        }else {
            for(int i = month1;i < month2;i++){
                if(i==2 || i==3 || i==5 || i==7 || i==11){
                    total += array[i-1];
                }else {
                    total += array[i-1]*2;
                }
            }
        }
        if(isYear(year2)&&month2>=3){
            total++;
        }
        if(month2==2 || month2==3 || month2==5 || month2==7 || month2==11){
            total += day2;
        }else {
            total += day2*2;
        }
        if(year2 > year1){
            total = total + year1Count(year1,month1,day1);
        }
        return total;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int year1 = sc.nextInt();
            int month1 = sc.nextInt();
            int day1 = sc.nextInt();
            int year2 = sc.nextInt();
            int month2 = sc.nextInt();
            int day2 = sc.nextInt();
            System.out.println(method(year1,month1,day1,year2,month2,day2));
        }
    }
}
