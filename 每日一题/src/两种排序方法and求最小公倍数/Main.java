package 两种排序方法and求最小公倍数;

import java.util.Scanner;

//两种排序方法
public class Main {
    //长度排序，直接比较长度即可
    public static boolean isLong(String[] array){
        if(array.length==1){
            return true;
        }
        for(int i = 1;i < array.length;i++){
            if(!(array[i].length() >= array[i-1].length())){
                return false;
            }
        }
        return true;
    }
    //字典排序，使用compareTo方法
    public static boolean isZiDian(String[] array){
        if(array.length == 1){
            return true;
        }
        for(int i = 1;i < array.length;i++){
            if(array[i].compareTo(array[i-1])>=0){
                continue;
            }else {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String nt = sc.nextLine();
        int n = Integer.valueOf(nt);
        String[] array = new String[n];
        for(int i = 0;i < n;i++){
            array[i] = sc.nextLine();
        }
        if(isLong(array)&&isZiDian(array)){
            System.out.println("both");
        }else if(isLong(array)){
            System.out.println("lengths");
        }else if(isZiDian(array)){
            System.out.println("lexicographically");
        }else {
            System.out.println("none");
        }
    }
}
