package 两种排序方法and求最小公倍数;

import java.util.Scanner;

public class Main1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        if(a > b){  //保证a小，b大，方便计算
            int t = a;
            a = b;
            b = t;
        }
        if(b % a == 0){
            System.out.println(b);
            return; //如果有打印，立即结束，避免影响最后输出
        }
        for(long i = b;i <= a*b;i++){
            if(i%a==0 && i%b==0){
                System.out.println(i);
                break;
            }
        }
    }
}
