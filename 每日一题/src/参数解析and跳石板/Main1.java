package 参数解析and跳石板;

import java.util.*;

//跳石板
public class Main1{
    //求约数
    public static List<Integer> method(int num){
        List<Integer> list = new ArrayList<>();
        for(int i = 2;i*i <= num;i++){
            if(num % i == 0){
                list.add(i);
                if(num/i!=i){
                    list.add(num/i);
                }
            }
        }
        return list;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] step = new int[m+1]; //从1到m共有m+1个位置
        for(int i = 0;i < m+1;i++){
            step[i] = Integer.MAX_VALUE;
        }
        step[n] = 0;
        for(int i = n;i < m;i++){
            if(step[i] == Integer.MAX_VALUE){
                continue;
            }
            List<Integer> list = method(i);
            for(int j : list){
                if(i+j <= m){
                    step[i+j] = Math.min(step[i+j],step[i]+1);
                }
            }
        }
        if(step[m] == Integer.MAX_VALUE){
            System.out.println(-1);
        }else {
            System.out.println(step[m]);
        }
    }
}