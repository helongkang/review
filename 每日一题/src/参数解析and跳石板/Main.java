package 参数解析and跳石板;

import java.util.*;
//参数解析
public class Main{
    public static void method(String s){
        StringBuilder sb = new StringBuilder();
        List<String> list = new ArrayList<>();
        boolean flag = false; //true表示引号内 false表示引号外
        for(int i = 0;i < s.length();i++){
            char c = s.charAt(i);
            if(c=='"'){
                flag = flag?false:true;
            }else if(c==' ' && !flag){  //如果是引号外遇到空格则记录字符串
                list.add(sb.toString());
                sb = new StringBuilder();
            }else {  //引号内的所有字符都添加
                sb.append(c);
            }
        }
        list.add(sb.toString()); //将最后一个字符串添加进去
        System.out.println(list.size());
        for(String e : list){
            System.out.println(e);
        }
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        method(s);
    }
}
