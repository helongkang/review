package 二叉树;

class Node{
    Node left;
    Node right;
    Node next;
}
public class LeetCode116 {
    public Node connect(Node root) {
        if(root == null){
            return root;
        }
        traverse(root.left,root.right);
        return root;
    }
    public void traverse(Node left,Node right){
        if(left==null || right==null){
            return;
        }
        left.next = right;
        traverse(left.left,left.right);
        traverse(right.left,right.right);
        traverse(left.right,right.left);
    }
}
