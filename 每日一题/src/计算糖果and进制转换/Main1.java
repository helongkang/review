package 计算糖果and进制转换;

import java.util.Scanner;

//进制准换
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        String array = "0123456789ABCDEF";
        boolean flag = false; //flag为true时，表示m为负数
        StringBuilder sb = new StringBuilder();
        if(m == 0){
            sb.append(0);
        }
        if(m < 0){
            m = -m;
            flag = true;
        }
        while(m != 0){
            sb.append(array.charAt(m%n));
            m /= n;
        }
        if(flag){
            sb.append("-");
        }
        System.out.println(sb.reverse());
    }
}
