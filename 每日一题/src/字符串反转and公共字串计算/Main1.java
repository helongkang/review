package 字符串反转and公共字串计算;

//公共字串计算
import java.util.*;
public class Main1{
    public static int method(String s1,String s2){
        if(s1.length() > s2.length()){
            String t = s1;
            s1 = s2;
            s2 = t;
        }
        if(s1.length()==0 || s2.length()==0){
            return 0;
        }
        int maxLen = 0;
        for(int i = 0;i < s1.length();i++){
            for(int j = i+1;j < s1.length();j++){
                if(s2.contains(s1.substring(i,j)) && j-i>maxLen){
                    maxLen = j-i;
                }
            }
        }

        return maxLen;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String s1 = sc.nextLine();
        String s2 = sc.nextLine();
        System.out.println(method(s1,s2));
    }
}