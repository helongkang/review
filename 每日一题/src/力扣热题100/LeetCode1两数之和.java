package 力扣热题100;

import java.util.HashMap;
import java.util.Map;

public class LeetCode1两数之和 {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> m = new HashMap<>();
        int[] ret = new int[2];
        for(int i = 0;i < nums.length;i++){
            if(m.containsKey(target-nums[i])){
                ret[0] = m.get(target-nums[i]);
                ret[1] = i;
                return ret;
            }
            m.put(nums[i],i);
        }
        return ret;
    }
}
