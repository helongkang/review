package 力扣热题100;

//删除链表倒数第N个结点
public class 力扣19 {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode fast = head;
        ListNode slow = head;
        ListNode pre = null;
        while(n > 0){
            fast = fast.next;
            n--;
        }
        while(fast != null){
            fast = fast.next;
            pre = slow;
            slow = slow.next;
        }
        //注意：区分删除第一个和其他
        if(slow == head){
            head = head.next;
        }else {
            pre.next = slow.next;
        }
        return head;
    }
}
