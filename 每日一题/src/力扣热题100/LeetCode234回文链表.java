package 力扣热题100;

//转换成数组处理
public class LeetCode234回文链表 {
    public boolean isPalindrome(ListNode head) {
        ListNode cur = head;
        int len = 0;
        while(cur != null){
            len++;
            cur = cur.next;
        }
        int[] arr = new int[len];
        int i = 0;
        cur = head;
        while(cur != null){
            arr[i++] = cur.val;
            cur = cur.next;
        }
        return isHuiWen(arr);
    }
    public boolean isHuiWen(int[] arr){
        int left = 0;
        int right = arr.length-1;
        while(left < right){
            if(arr[left] == arr[right]){
                left++;
                right--;
            }else {
                return false;
            }
        }
        return true;
    }
}
