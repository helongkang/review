package 力扣热题100;

public class LeetCode141环形链表 {
    public boolean hasCycle(ListNode head) {
        if(head == null){
            return false;
        }
        ListNode fast = head;
        ListNode slow = head;
        while(fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
            if(fast == null){
                return false;
            }
            if(fast == slow){
                return true;
            }
        }
        return false;
    }
}
