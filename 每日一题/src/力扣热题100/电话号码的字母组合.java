package 力扣热题100;

import java.util.LinkedList;
import java.util.List;

public class 电话号码的字母组合 {
    String[] arr = {"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    List<String> ret = new LinkedList<>();
    public List<String> letterCombinations(String digits) {
        if(digits.length() == 0){
            return ret;
        }
        backtrack(digits,0,new StringBuilder());
        return ret;
    }
    public void backtrack(String digits,int start,StringBuilder sb){
        //sb中保存的路径最多不超过digits的长度，若长度相等则走到路径尽头了，将路径保存在ret里
        if(sb.length() == digits.length()){
            ret.add(sb.toString());
            return;
        }
        for(int i = start; i < digits.length();i++){
            int pos = digits.charAt(i) - '0';
            for(char c : arr[pos].toCharArray()){
                sb.append(c);
                backtrack(digits,i+1,sb);
                sb.deleteCharAt(sb.length()-1);
            }
        }
    }
}
