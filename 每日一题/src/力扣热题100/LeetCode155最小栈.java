package 力扣热题100;

import java.util.Stack;

public class LeetCode155最小栈 {
    Stack<Integer> stack1;
    Stack<Integer> stack2;
    public LeetCode155最小栈() {
        stack1 = new Stack<>();
        stack2 = new Stack<>();
    }

    public void push(int val) {
        if(stack1.empty() || val<=stack2.peek()){
            stack2.push(val);
        }
        stack1.push(val);
    }

    public void pop() {
        if(stack1.peek().equals(stack2.peek())){
            stack1.pop();
            stack2.pop();
        }else {
            stack1.pop();
        }
    }

    public int top() {
        return stack1.peek();
    }

    public int getMin() {
        return stack2.peek();
    }
}
