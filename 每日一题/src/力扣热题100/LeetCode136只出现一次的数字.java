package 力扣热题100;

public class LeetCode136只出现一次的数字 {
    public int singleNumber(int[] nums) {
        int ret = 0;
        for(int i = 0;i < nums.length;i++){
            ret ^= nums[i];
        }
        return ret;
    }
}
