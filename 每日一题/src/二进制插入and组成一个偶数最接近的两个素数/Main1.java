package 二进制插入and组成一个偶数最接近的两个素数;

import java.util.*;


//查找组成一个偶数最接近的两个素数
//从该数的中间值往下递减，如果该值和对应的另一个值都为素数，则是我们要找的两个值
public class Main1{
    //判断是否是素数
    public static boolean isSuShu(int n){
        for(int i = 2;i <= n/2;i++){
            if(n%i==0){
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a1 = n/2;
        int i = 0;
        for(i = a1;i >= 1;i--){
            if(isSuShu(i) && isSuShu(n-i)){
                break;
            }
        }
        System.out.println(i);
        System.out.println(n-i);
    }
}
