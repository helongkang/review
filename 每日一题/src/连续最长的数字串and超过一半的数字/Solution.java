package 连续最长的数字串and超过一半的数字;


import java.util.*;

//数组中找出超过一半的数字
public class Solution {
    public int MoreThanHalfNum_Solution(int [] array) {
        int temp = array.length/2;
        Map<Integer,Integer> m = new HashMap<>();
        for(int i = 0;i < array.length;i++){
            m.put(array[i],m.getOrDefault(array[i],0)+1);
        }
        for(int i = 0;i < array.length;i++){
            if(m.get(array[i]) > temp){
                return array[i];
            }
        }
        return -1;
    }
}
