package 连续最长的数字串and超过一半的数字;


import java.util.*;
//字符串中找出连续最长的数字串
public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        StringBuilder sb = new StringBuilder();
        StringBuilder ret = new StringBuilder();
        for(int i = 0;i < s.length();i++){
            char c = s.charAt(i);
            if(c < '0' || c > '9'){
                if(ret.length() < sb.length()){
                    ret = sb;
                }
                sb = new StringBuilder();
            }else {
                sb.append(c);
            }
        }
        if(ret.length() < sb.length()){
            ret = sb;
        }
        System.out.println(ret.toString());
    }
}