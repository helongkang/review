package 汽水瓶and查找两个字符串ab中最长的公共子串;


import java.util.*;
//汽水瓶
public class Main{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = sc.nextInt();
            if(n == 0){
                return;
            }
            int count = 0;
            while(n/3 != 0){
                count += n/3;
                n = n/3+n%3;
            }
            if(n == 2){
                count += 1;
            }
            System.out.println(count);
        }
    }
}
