package 汽水瓶and查找两个字符串ab中最长的公共子串;

//查找零个字符串啊，b最长的公共子串
import java.util.*;
public class Main1{
    public static String method(String a,String b){
        String ret = "";
        int len = 0;
        for(int i = 0;i < a.length();i++){
            for(int j = i+1;j <= a.length();j++){
                if(b.contains(a.substring(i,j)) && j-i>len){
                    ret = a.substring(i,j);
                    len = j-i;
                }
            }
        }
        return ret;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        String b = sc.nextLine();
        //a小b大
        if(a.length() > b.length()){
            String t = a;
            a = b;
            b = t;
        }
        System.out.println(method(a,b));
    }
}