package 组队竞赛and删除公共字符;

import java.util.Scanner;

//删除公共字符
//判断s2中是否包含s1中每一个字符
public class Main {
    public static String method(String s1,String s2){
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i < s1.length();i++){
            if(!s2.contains(s1.substring(i,i+1))){
                sb.append(s1.charAt(i));
            }
        }
        return sb.toString();
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1 = sc.nextLine();
        String s2 = sc.nextLine();
        System.out.println(method(s1,s2));
    }
}
