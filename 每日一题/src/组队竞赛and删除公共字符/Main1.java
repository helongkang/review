package 组队竞赛and删除公共字符;

import java.util.Arrays;
import java.util.Scanner;

//组队竞赛
//组队规则：两个最大带一个最小
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        int n = Integer.valueOf(s);
        int[] array = new int[3*n];
        for(int i = 0;i < 3*n;i++){
            array[i] = sc.nextInt();
        }
        Arrays.sort(array);
        long sum = 0;
        for(int i = 0;i < n;i++){
            sum = sum+array[array.length-2*(i+1)];
        }
        System.out.println(sum);
    }
}
