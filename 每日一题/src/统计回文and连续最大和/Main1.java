package 统计回文and连续最大和;


import java.util.Scanner;

//连续最大和
public class Main1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] array = new int[n];
        for(int i = 0;i < n;i++){
            array[i] = sc.nextInt();
        }

        int max = array[0];
        int sum = array[0];
        for(int i = 1;i < array.length;i++){
            sum = Math.max(sum+array[i],array[i]);
            if(sum > max){
                max = sum;
            }
        }
        System.out.println(max);
    }
}
