package 统计回文and连续最大和;


import java.util.Scanner;

//统计回文
//写一个方法判断是否回文
//将这个字符串一次插入指定位置，如果满足回文，则计数加1
//返回该计数
//插入指定位置，用StringBuilder对象的insert(int,String)方法
public class Main {
    public static boolean isHuiWen(String s){
        StringBuilder sb = new StringBuilder(s);
        sb.reverse();
        return s.equals(sb.toString());
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        String b = sc.nextLine();
        int count = 0;

        for(int i = 0;i <= a.length();i++){
            StringBuilder sb = new StringBuilder(a);
            sb.insert(i,b);
            String s = sb.toString();
            if(isHuiWen(s)){
                count++;
            }
        }
        System.out.println(count);
    }
}
