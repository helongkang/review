package 斐波那契额数列and合法的括号判断;

import java.util.Stack;

public class Main1 {
    public boolean chkParenthesis(String A, int n) {
        if(n % 2 != 0){
            return false;
        }
        Stack<Character> stack = new Stack<>();
        for(int i = 0;i < n;i++){
            char c = A.charAt(i);
            if(c!='(' && c!=')'){   //不是括号直接返回false
                return false;
            }
            if(c == '('){     //左括号入栈
                stack.push(c);
            }
            if(c == ')'){    //右括号的话：
                if(stack.empty()){  //栈为空返回false
                    return false;
                }else {
                    stack.pop();   //否则出栈一个括号
                }
            }
        }
        if(!stack.empty()){  //如果栈不为空，返回false否则返回true
            return false;
        }else {
            return true;
        }
    }
}
