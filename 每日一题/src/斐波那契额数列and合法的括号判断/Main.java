package 斐波那契额数列and合法的括号判断;

import java.util.Scanner;

//斐波那契额数列
public class Main {
    public static int getFib(int n){ //获得第n个斐波那契额数
        if(n == 0){
            return 0;
        }
        if(n == 1){
            return 1;
        }
        return getFib(n-1)+getFib(n-2);
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int i = 0;
        for( i = 0;;i++){
            if(getFib(i) > n){ //找到第一个大于nd的斐波那契额数
                break;
            }
        }
        int a = getFib(i)-n;
        int b = n-getFib(i-1); //比较n两边哪个数离的近
        if(a > b){
            System.out.println(b);
        }else {
            System.out.println(a);
        }
    }
}
