package 计算日期到天数转换and幸运的袋子;

import java.util.*;
//计算日期到天数转换
public class Main{
    public static int method(int year,int month,int day){
        int[] array = {31,28,31,30,31,30,31,31,30,31,30,31};
        int ret = 0;
        ret += day;
        boolean flag = false;
        if((year%4==0 && year%100!=0) || year%400==0){
            flag = true;  //表示闰年
        }
        for(int i = 0;i < month-1;i++){
            ret += array[i];
        }
        if(flag&&month>=3){
            ret += 1;
        }
        return ret;
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        int month = sc.nextInt();
        int day = sc.nextInt();
        System.out.println(method(year,month,day));
    }
}
