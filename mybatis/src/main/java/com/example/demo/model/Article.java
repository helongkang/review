package com.example.demo.model;

import lombok.Data;

@Data
public class Article {
    private Integer id;
    private String content;
    private Integer userId;
    private User user;
}
