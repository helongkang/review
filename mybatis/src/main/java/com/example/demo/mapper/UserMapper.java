package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

//Mapper会注册当前接口为一个bean对象
@Mapper
public interface UserMapper {
    //根据名称进行模糊查询
    List<User> getByName(@Param("username") String username);

    //根据用户id查询用户
    User getUserById( @Param("id")Integer id);

    //根据用户id修改用户名
    int updateById(@Param("id") Integer id,@Param("username") String username);

    //根据用户id删除用户
    int deleteById(@Param("id") Integer id);

    //插入对象，返回受影响行数
    int insertOne(User user);

    //插入对象，返回主键
    int addOne(User user);

    //插入可以传入密码也可以不传入密码的用户
    int addTest(User user);

    //使用where标签，根据id查询用户
    List<User> getByIdWhere(@Param("id") Integer id);

    //根据id集合，删除对用的用户
    int deleteByIds(List<Integer> ids);
}
