package com.example.demo.mapper;

import com.example.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest //表示当前单元测试运行在spring boot 环境中
@Slf4j
class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    @Test
    @Transactional //不污染数据库
    void getUserById() {
        User user = userMapper.getUserById(1);
        Assertions.assertNotNull(user);
    }

    @Test
    @Transactional//单元测试中添加此注解，在方法执行完会发生回滚
    void updateById() {
        int n = userMapper.updateById(1,"老三");
        Assertions.assertEquals(1,n);
    }

    @Test
    @Transactional
    void deleteById() {
        int n = userMapper.deleteById(1);
        Assertions.assertEquals(1,n);
    }

    @Test
    void insertOne() {
        User user = new User();
        user.setId(5);
        user.setUsername("元帅");
        user.setPassword("456");
        int n = userMapper.insertOne(user);
        Assertions.assertEquals(1,n);
    }

    @Test
    void addOne() {
        User user = new User();
        user.setUsername("将军");
        user.setPassword("567");
        int n = userMapper.addOne(user);
        System.out.println("新增用户主键："+user.getId());
    }

    @Test
    void getByName() {
        String username = "张";
        List<User> list = userMapper.getByName(username);
        log.info("查询到的用户列表："+list);
    }

    @Test
    void addTest() {
        User user = new User();
        user.setUsername("爱国");
        int n = userMapper.addTest(user);
        log.info("插入的用户："+user);
    }

    @Test
    void getByIdWhere() {
        List<User> list = userMapper.getByIdWhere(null);
        log.info("查询到的用户："+list);
    }

    @Test
    void deleteByIds() {
        List<Integer> ids = new ArrayList<>();
        ids.add(6);
        ids.add(7);
        ids.add(8);
        int n = userMapper.deleteByIds(ids);
    }
}