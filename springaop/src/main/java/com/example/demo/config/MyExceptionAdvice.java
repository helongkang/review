package com.example.demo.config;


import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class MyExceptionAdvice {

    @ExceptionHandler(ArithmeticException.class)
    public Object handler(Exception e){
        Map<String,Object> result = new HashMap<>();
        result.put("success",0);
        result.put("status",-1);
        result.put("msg",e.getMessage());
        return result;
    }
}
