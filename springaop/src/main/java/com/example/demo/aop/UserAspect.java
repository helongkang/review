package com.example.demo.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect //当时当前类为一个切面
@Component
public class UserAspect {

    //定义一个切点，设置拦截规则
    //拦截包名，拦截类名，*拦截所有的方法，（..）是所有参数
    @Pointcut("execution(* com.example.demo.controller.UserController.*(..))")
    public void pointcut(){
    }

    //定义pointcut切点的前置通知，在执行目标方法前执的方法就是前置通知
    @Before("pointcut()") //括号中的为切点的方法名称
    public void doBefore(){
        System.out.println("执行前置通知");
    }

    //后置通知
    @After("pointcut()")
    public void doAfter(){
        System.out.println("执行后置通知");
    }

    //返回后通知
    @AfterReturning("pointcut()")
    public void doReturn(){
        System.out.println("执行返回后通知");
    }

    //抛出异常后
    @AfterThrowing("pointcut()")
    public void doAfterThrow(){
        System.out.println("执行了抛出异常后通知");
    }

    //环绕通知
    @Around("pointcut()")
    public Object doAround(ProceedingJoinPoint joinPoint){
        Object result = null;
        StopWatch stopWatch = new StopWatch();
        long start = System.currentTimeMillis();
        try {
            stopWatch.start();
            //执行目标方法以及执行目标方法相应的通知
            result = joinPoint.proceed();
            stopWatch.stop();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        //joinPoint.getSignature().getName()获得执行的方法名
        System.out.println(joinPoint.getSignature().getName()+"执行了："+stopWatch.getTotalTimeMillis()+"ms");
        return result;
    }
}
