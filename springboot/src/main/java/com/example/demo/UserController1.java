package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class UserController1 {
    //先得到日志对象
    private final static Logger log = LoggerFactory.getLogger(UserController1.class);

    @RequestMapping("/hi")
    public void sayHi(){
        //使用日志对象提供的方法打印日志
        log.trace("我是trace");
        log.debug("我是debug");
        log.info("我是info");
        log.warn("我是warn");
        log.error("我是error");
    }
}
