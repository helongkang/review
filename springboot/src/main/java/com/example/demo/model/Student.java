package com.example.demo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

//读取yml中的对象
@Data //提供Setter，Getter，toString
@ConfigurationProperties(prefix = "student1")
@Component
public class Student {
    private int id;
    private String name;
}
