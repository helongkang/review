package com.example.demo;

import com.example.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

    @Value("${server.port}")
    private Integer port;

    @Value("${person}")
    private String person;

    @Autowired
    private Student student;


    @ResponseBody
    @RequestMapping("/hello")
    public String sayHi(){
        return "打印："+student;
    }
}
