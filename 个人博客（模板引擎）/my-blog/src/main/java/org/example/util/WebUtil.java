package org.example.util;

import org.example.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class WebUtil {
    public static User checkLogin(HttpServletRequest req){
        User user = null;
        HttpSession session = req.getSession(false);
        user = (User) session.getAttribute("user");
        return user;
    }
}
