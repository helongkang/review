package org.example.api;

import org.example.dao.ArticleDao;
import org.example.model.Article;
import org.example.model.User;
import org.example.util.WebUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/blog_add")//添加文章
public class BlogAddServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = WebUtil.checkLogin(req);
        if(user == null){
            //未登录不允许访问
            resp.sendRedirect("login.html");
            return;
        }
        //解析请求
        req.setCharacterEncoding("utf-8");
        //获取文章
        String content = req.getParameter("content");
        //获取标题
        String title = req.getParameter("title");
        //数据库插入一条数据，相当于插入一个对象
        Article beInsert = new Article();
        beInsert.setContent(content);
        beInsert.setTitle(title);
        beInsert.setUserId(user.getId());
        beInsert.setDate(new java.util.Date());
        int n = ArticleDao.insertOne(beInsert);
        resp.sendRedirect("blog_list");
    }
}
