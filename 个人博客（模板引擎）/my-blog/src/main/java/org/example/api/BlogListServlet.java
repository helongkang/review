package org.example.api;

import org.example.dao.ArticleDao;
import org.example.model.Article;
import org.example.model.User;
import org.example.util.DBUtil;
import org.example.util.WebUtil;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/blog_list")//博客列表
public class BlogListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //未登录，访问跳转到登陆页面
        User user = WebUtil.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login.html");
            return; //未登录，直接跳转，不会执行后边逻辑
        }
        //通过登陆用户id查找所有文章
        List<Article> articles = ArticleDao.selectById(user.getId());
        //通过登陆用户id查找文章数目
        int count = ArticleDao.getCount(user.getId());
        //返回响应
        ServletContext sc = getServletContext();
        TemplateEngine engine = (TemplateEngine)sc.getAttribute("engine");
        WebContext wc = new WebContext(req,resp,sc);
        wc.setVariable("nickname",user.getNickname()); //设置动态参数，nickname从session中保存的user获取
        wc.setVariable("articles",articles); //articles为通过用户id查到的所有文章
        wc.setVariable("count",count);
        String html = engine.process("blog_list",wc);
        resp.setContentType("text/html; charset=utf-8");
        resp.getWriter().write(html);
    }
}
