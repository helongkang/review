package org.example.api;

import org.example.dao.UserDao;
import org.example.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")//登陆
public class LoginServlet extends HttpServlet {
    //登陆功能，表单格式提交，校验账号密码
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //解析请求
        req.setCharacterEncoding("utf-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //在数据库校验账号密码：通过账号密码在数据库查用户，若能查到则账号密码正确
        User user = UserDao.isLogin(username,password);
        if(user != null){
            //登陆成功，设置session，重定向到博客列表页面
            HttpSession session = req.getSession(true);
            session.setAttribute("user",user);
            resp.sendRedirect("blog_list");
        }else {
            //登陆失败，继续在登陆页面
            resp.sendRedirect("login.html");
        }
    }
}
