package org.example.api;

import org.example.model.User;
import org.example.util.WebUtil;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/blog_edit")//博客编辑页面
public class BlogEditServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //是否登陆
        User user = WebUtil.checkLogin(req);
        if(user == null){
            //未登录跳转到用户登录页面
            resp.sendRedirect("login.html");
        }else {
            //登陆成功，博客编辑页面没有动态元素，所以通过引擎直接整合模板，不用设置动态参数
            ServletContext sc = req.getServletContext();
            TemplateEngine engine = (TemplateEngine) sc.getAttribute("engine");
            WebContext wc = new WebContext(req,resp,sc);
            String html = engine.process("blog_edit",wc);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write(html);
        }
    }
}
