package org.example.dao;

import org.example.model.User;
import org.example.util.DBUtil;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    public static User isLogin(String username, String password){
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = DBUtil.getConnection();
            String sql = "select * from user where username=? and password=?";
            ps = c.prepareStatement(sql);
            ps.setString(1,username);
            ps.setString(2,password);
            rs = ps.executeQuery();
            User user = null;
            while(rs.next()){
                user = new User();
                int id = rs.getInt("id");
                String nickname = rs.getString("nickname");
                user.setId(id);
                user.setNickname(nickname);
                user.setUsername(username);
                user.setPassword(password);
            }
            return user;
        } catch (SQLException e) {
            throw new RuntimeException("校验账号密码出错",e);
        } finally {
            DBUtil.close(c,ps,rs);
        }
    }

    @Test
    public void testLogin(){
        System.out.println(isLogin("abc","123"));
    }
}
