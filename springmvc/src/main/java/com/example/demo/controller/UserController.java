package com.example.demo.controller;


import com.example.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Controller
@RequestMapping("/user")
@ResponseBody
@Slf4j
public class UserController {

    @Value("${img.path}")
    private String imgPath;



    @RequestMapping("/hello1")
    public String hello1(){
        return "hello11";
    }

    @RequestMapping(method = RequestMethod.POST,value = "/hello2")
    public String hello2(){
        return "hello2";
    }

    @PostMapping("/hello3")
    public String hello3(){
        return "hello3";
    }

    @RequestMapping("/getuser1")
    public User getUserById(Integer id){ //获取单个参数，这里参数名字一定要和前端传递过来的参数一致
        User user = new User();
        user.setId(id);
        user.setName("张三");
        return user;
    }

    @RequestMapping("/login1")
    public String login1(String username,String password){
        return "用户名："+username+","+"密码："+password;
    }

    @RequestMapping("/register")
    public User register(@RequestBody User user){
        return user;
    }

    @RequestMapping("/getname")
    //@RequestParam是参数重命名
    public String getName(@RequestParam("ua") String name){
        return name;
    }

    @RequestMapping("/getuser2/{username}/{password}")
    public String getUser(@PathVariable String username,@PathVariable String password){
        return "用户名："+username+"密码："+password;
    }

    @RequestMapping("/upimg")
    public boolean upImg(Integer id, @RequestPart("img") MultipartFile file){
        boolean ret = false;
        //目录imgpath
        //图片名称UUID
        //获取原上传图片格式
        String fileName = file.getOriginalFilename();
        fileName = fileName.substring(fileName.lastIndexOf(".")); //获取图片后缀格式，png
        fileName = UUID.randomUUID().toString()+fileName;

        try {
            file.transferTo(new File(imgPath+fileName));
            ret = true;
        } catch (IOException e) {
            log.error("上传图像失败"+e.getMessage());
        }
        return ret;
    }

    @RequestMapping("/cookie")
    public void getCookie(HttpServletRequest req){
        Cookie[] cookies = req.getCookies();
        for(Cookie c : cookies){
            log.info(c.getName()+c.getValue());
        }
    }
}
