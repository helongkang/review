package com.example.demo.controller;

import com.example.demo.model.Student;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;


@RestController
public class LoginController {
    @RequestMapping("/login")
    public HashMap<String,Object> login(@RequestBody Student student){ //前端json格式只能通过对象来接收
        HashMap<String,Object> ret = new HashMap<>();
        if(student.getUsername().equals("abc") && student.getPassword().equals("123")){
            ret.put("ok",true);
        }else {
            ret.put("ok",false);
        }
        return ret;
    }
}
