package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalcController {

    @RequestMapping("/calc")
    public String calc(Integer num1,Integer num2){
        if(num1==null || num2==null){
            return "<h3>参数错误</h3>";
        }
        return "<h3>"+"结果："+(num1+num2)+"</h3>";
    }
}
