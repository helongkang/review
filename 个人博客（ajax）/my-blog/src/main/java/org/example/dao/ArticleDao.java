package org.example.dao;

import org.example.model.Article;
import org.example.util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ArticleDao {
    //通过用户id查询所有文章
    public static List<Article> selectById(Integer id){
        List<Article> articles = new ArrayList<>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            c = DBUtil.getConnection();
            String sql = "select * from article where user_id=?";
            ps = c.prepareStatement(sql);
            ps.setInt(1,id);
            rs = ps.executeQuery();
            while(rs.next()){
                Article a = new Article();
                a.setId(rs.getInt("id"));
                a.setTitle(rs.getString("title"));
                java.sql.Date date = rs.getDate("date");
                long time = date.getTime();
                a.setDate(new java.util.Date(time));
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String dateString = df.format(a.getDate());
                a.setDateString(dateString);
                String content = rs.getString("content");
                a.setContent(content.length()>50 ? content.substring(0,50) : content);
                a.setUserId(id);
                articles.add(a);
            }
            return articles;
        } catch (SQLException e) {
            throw new RuntimeException("查询文章出错",e);
        } finally {
            DBUtil.close(c,ps,rs);
        }
    }

    //根据文章id查文章
    public static Article queryById(int id) {
        Article a = null;
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            c = DBUtil.getConnection();
            String sql = "select * from article where id=?";
            ps = c.prepareStatement(sql);
            ps.setInt(1,id);
            rs = ps.executeQuery();
            while(rs.next()){
                a = new Article();
                a.setId(id);
                a.setTitle(rs.getString("title"));
                java.sql.Date date = rs.getDate("date");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String dateString = df.format(new java.util.Date(date.getTime()));
                a.setDateString(dateString);
                a.setContent(rs.getString("content"));
                a.setUserId(rs.getInt("user_id"));
            }
            return a;
        } catch (SQLException throwables) {
            throw new RuntimeException("查询文章详情jdbc出错",throwables);
        } finally{
            DBUtil.close(c,ps,rs);
        }
    }

    //插入文章
    public static int insertOne(Article a) {
        Connection c = null;
        PreparedStatement ps = null;
        try{
            c = DBUtil.getConnection();
            String sql = "insert into article(title,`date`,content,user_id) values(?,?,?,?)";
            ps = c.prepareStatement(sql);
            ps.setString(1,a.getTitle());
            //ps.setDate(2,new java.sql.Date(a.getDate().getTime()))
            ps.setDate(2,new java.sql.Date(System.currentTimeMillis()));
            ps.setString(3,a.getContent());
            ps.setInt(4,a.getUserId());
            return ps.executeUpdate();
        } catch (SQLException throwables) {
            throw new RuntimeException("发布文章jdbc出错",throwables);
        } finally {
            DBUtil.close(c,ps);
        }
    }

    public static int getCount(Integer id) {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            c = DBUtil.getConnection();
            String sql = "select 0 from article where user_id=?";
            ps = c.prepareStatement(sql);
            ps.setInt(1,id);
            rs = ps.executeQuery();
            int count = 0;
            while(rs.next()){
                count++;
            }
            return count;
        } catch (SQLException throwables) {
            throw new RuntimeException("查询文章数目出错",throwables);
        } finally{
            DBUtil.close(c,ps,rs);
        }
    }
}
