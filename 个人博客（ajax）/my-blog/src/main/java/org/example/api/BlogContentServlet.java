package org.example.api;

import org.example.dao.ArticleDao;
import org.example.model.Article;
import org.example.model.JsonResult;
import org.example.model.User;
import org.example.util.WebUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/blog_content")//博客详情
public class BlogContentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //未登录，访问跳转到登陆页面
        User user = WebUtil.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login.html");
            return; //未登录，直接跳转，不会执行后边逻辑
        }

        //路径为：blog_content?id=文章id
        //解析请求
        String sid = req.getParameter("id"); //获取到文章id
        //通过文章id查询整篇文章
        Article a = ArticleDao.queryById(Integer.parseInt(sid));
        int count = ArticleDao.getCount(user.getId());

        JsonResult json = new JsonResult();
        json.setOk(true);
        Map<String,Object> data = new HashMap<>();
        data.put("nickname",user.getNickname());
        data.put("article",a);
        data.put("count",count);
        json.setData(data);
        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write(WebUtil.write(json));
    }
}
