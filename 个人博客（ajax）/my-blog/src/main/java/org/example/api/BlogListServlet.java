package org.example.api;

import org.example.dao.ArticleDao;
import org.example.model.Article;
import org.example.model.JsonResult;
import org.example.model.User;
import org.example.util.WebUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/blog_list")//博客列表
public class BlogListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //未登录，访问跳转到登陆页面
        User user = WebUtil.checkLogin(req);
        if(user == null){
            resp.sendRedirect("login.html");
            return; //未登录，直接跳转，不会执行后边逻辑
        }
        //通过登陆用户id查找所有文章
        List<Article> articles = ArticleDao.selectById(user.getId());
        //通过登陆用户id查找文章数目
        int count = ArticleDao.getCount(user.getId());

        //先构造响应正文需要的java对象，再转化为json字符串，再设置到响应正文
        JsonResult json = new JsonResult();
        json.setOk(true);
        //前端需要的数据有nickname，articles，可以用map保存，然后设置到json.data中
        Map<String,Object> data = new HashMap<>();
        data.put("nickname",user.getNickname());
        data.put("articles",articles);
        data.put("count",count);
        json.setData(data);

        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write(WebUtil.write(json));
    }
}
