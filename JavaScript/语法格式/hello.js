
// alert("hello word!");
// var a = 10;
// var name = "小张";
// a = 20;
// console.log(name);
// var name = prompt("请输入姓名：");
// var age = prompt("请输入年龄：");
// var gender = prompt("请输入性别：");
// alert("姓名："+name+"\n"+"年龄："+age+"\n"+"性别："+gender);
// 
// var a = 10;  //十进制
// var b = 07;  //八进制，以0开头
// var c = 0xa; //十六进制，以0x开头
// var d = 0b10;//二进制，以0b开头
// var max = Number.MAX_VALUE;
// console.log(max*2); //得到Infinity
// console.log(-max*2);//得到-Infinity
// console.log("haha"-10);//得到NaN
// console.log(isNaN(10)); //true
// console.log(isNaN("haha"-10));  //false
// var a = "aaa";
// var b = 'bbb';
// var c = ccc;  //运行出错
// var msg = "my name is 'zhangsan'"; //正确，双引号包裹单引号
// var msg = 'my name is "zhangsan"'; //正确，单引号包裹双引号
// var msg = "my name is \"zhangsan\""; //正确，使用转义字符，\"表示内部的引号
// var msg = "my name is "zhangsan""; //出错
// var s = "haha";
// console.log(s.length); //4
// var c = "hello";
// console.log(c.length); //5
// var a = "hello";
// var b = "word";
// console.log(a+b);//helloword
// var a = "my age is ";
// var b = 16;
// console.log(a+b); //my age is 16
// var a = true;
// var b = false;
// console.log(2+a+b);//3
// var a;
// console.log(a);//undefined
// var a;
// console.log(a+"haha"); //undefinedhaha
// var a;
// console.log(a+10);//NaN
// var a = null;
// console.log(a+10); //10
// console.log(a+"haha"); //nullhaha
// console.log(2&&3); //3
// console.log(3&&0); //0
// console.log(2||3); //2
// console.log(0||3); //3
// var num = 10;
// if(num == 1){
//     console.log(num+"既不是偶数也不是奇数");
// }else if(num % 2 == 0){
//     console.log(num+"是偶数");
// }else {
//     console.log(num+"是奇数");
// }
// var a = true;
// a = a?false:true;
// console.log(a);
// var day = prompt("请输入一个数字：");
// switch(parseInt(day)){ //day为一个字符串，必须转为数字
//     case 1:
//         alert("星期一");
//         break;
//     case 2:
//         alert("星期二");
//         break;
//     case 3:
//         alert("星期三");
//         break;
//     case 4:
//         alert("星期四");
//         break;
//     case 5:
//         alert("星期五");
//         break;
//     case 6:
//         alert("星期六");
//         break;
//     case 7:
//         alert("星期天");
//         break;
//     default:
//         alert("输入有误");   
// }
// var num = 1;
// while(num <= 10){
//     console.log(num);
//     num++;
// }
// var result = 1;
// for(var i = 1;i <= 5;i++){
//     result *= i;
// }
// console.log(result);
//var arr = new Array();
// var arr = [1,2,3,"hehe",5,true];
// console.log(arr[0]);
// console.log(arr[3]);
// console.log(arr[5]);
// console.log(arr[100]);
// var arr = [1,2,3,4];
// arr.length = 6;
// console.log(arr[5]); //新增元素默认值为undefined
// console.log(arr);
// var arr = [];
// arr[2] = 3;
// console.log(arr);
// console.log(arr[0]); //[0]和[1]都是undefined
// var arr = [1,2,3,4,5,6,7,8,9];
// var newArr = [];
// for(var i = 0;i < arr.length;i++){
//     if(arr[i] == 1){
//         continue;
//     }
//     if(arr[i] % 2 != 0){
//         newArr.push(arr[i]);
//     }
// }
// console.log(newArr);
// var arr = [1,2,3,4,5];
// arr.splice(2,2);//删除3，4
// console.log(arr);//打印1，2，5
// function 函数名 (参数列表) {
//     函数体;
//     return 返回值;
// }
// function add1(a,b){ //带有返回值的函数
//     return a+b;
// }
// function add2(a,b){ //没有返回值的函数
//     console.log(a+b);
// }
// console.log(add1(1,2));
// add2(3,4);
// function sum(a,b,c){
//     console.log(a+b);
// }
// sum(10,20,30); //10+20 = 30
// sum(10); //10+undefined = NaN
// var add = function(){
//     var sum = 0;
//     for(var i = 0;i < arguments.length;i++){
//         sum += arguments[i];
//     }
//     return sum;
// }
// console.log(add(10,20,30));//60
// console.log(add(1,2,3));//6
// console.log(typeof add);//function
// var a = 10;//全局变量
// function test (){
//     var a = 20;//局部变量
//     console.log(a);//优先找自己的局部变量，若没有则找全局变量
// }
// test();

// function test(){
//     var a = 10;
//     let b = 20;
// }
// console.log(b); //报错
// console.log(a); //报错
// function test(){
//     a = 10;
//     b = 20;
// }
// test();
// console.log(a);
// console.log(b);
// var student = {
//     name: "张三",
//     age: 23,
//     sex: "男",
//     play: function(){
//         console.log("正在玩耍");
//     }
// };
// console.log(student.name);
// console.log(student.age);
// student.play();//调用方法要加()
// var student = new Object();
// student.name = "牛魔王";
// student.age = 500;
// student.play = function(){
//     console.log("正在吃唐僧肉");
// }
// console.log(student.name);
// console.log(student.age);
// student.play();
// student.height = 230;
// function 构造函数名(参数列表){
//     this.属性 = 值;
//     this.方法 = function...
// }
// var obj = new 构造函数名(实参);
function Cat(name,color,type,sound){
    this.name = name;
    this.color = color;
    this.type = type;
    this.miao = function(){
        console.log(sound);
    }
}
var nuomi = new Cat("糯米","白色","波斯猫","喵喵~");
var wanzi = new Cat("丸子","灰色","中华田园猫","咪咪");
var tuanzi = new Cat("团子","黑色","狸猫","喵呜");
console.log(wanzi);
nuomi.miao();