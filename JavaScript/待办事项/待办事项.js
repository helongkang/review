//新建按钮绑定点击事件
var add = document.querySelector("#addDiv>button");
var text = document.querySelector("#addDiv>input");
var done = document.querySelector("#done");
add.onclick = function(){
    var addText = text.value
    //首尾去除空格，如果没有输入内容，直接返回
    if(addText.trim().length==0) return;
    //有内容再添加
    var todo = document.querySelector("#todo");
    //构造添加内容
    var row = document.createElement("div");
    row.className = "row";
    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    row.appendChild(checkbox);
    var span = document.createElement("span");
    span.innerText = addText;
    row.appendChild(span);
    var button = document.createElement("button");
    button.innerText = "删除";
    row.appendChild(button);
    todo.appendChild(row);

    //复选框，绑定改变事件，如果选中在已完成，未选中在未完成
    checkbox.onchange = function(){
        var target = checkbox.checked?done:todo;
        target.appendChild(row);
    }
    button.onclick = function(){
        var parent = this.parentNode;
        var grand = parent.parentNode;
        grand.removeChild(parent);
    }
}