package 阻塞队列;

import java.util.Arrays;

public class MyBlockingQueue {
    private int[] tasks;
    private int size;
    private int head;
    private int tail;

    public MyBlockingQueue(int size){
        tasks = new int[size];
    }

    public void put(int e) throws InterruptedException {
        synchronized (this){
            if(size == tasks.length){
                this.wait();
            }
            tasks[tail] = e;
            tail++;
            if(tail == tasks.length){
                tail = 0;
            }
            size++;
            this.notify(); //没有人等待，唤醒无副作用
        }
    }

    public synchronized Integer take() throws InterruptedException {
        if(size == 0){
            this.wait();
        }
        int ret = tasks[head];
        tasks[head] = 0;
        head++;
        if(head == tasks.length){
            head = 0;
        }
        size--;
        this.notify();
        return ret;
    }


    @Override
    public String toString() {
        return "MyBlockingQueue{" +
                "tasks=" + Arrays.toString(tasks) +
                ", size=" + size +
                ", head=" + head +
                ", tail=" + tail +
                '}';
    }

    public static void main(String[] args) throws InterruptedException {
        MyBlockingQueue q = new MyBlockingQueue(3);
        q.put(1);
        q.put(2);
        q.put(3);
        int a = q.take();
        int b = q.take();
        System.out.println(q);
    }
}
