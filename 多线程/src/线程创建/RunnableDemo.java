package 线程创建;

public class RunnableDemo implements Runnable{
    @Override
    public void run() {
        System.out.println("实现Runnable接口方式创建线程成功");
    }

    public static void main(String[] args) {
       // new Thread(new RunnableDemo()).start();
        //使用匿名内部类方式->lambda表达式方式
        new Thread(() -> System.out.println("使用匿名内部类方式创建线程成功"),"线程1").start(); //第二个参数可以指定线程名称
    }
}
