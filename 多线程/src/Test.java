
class A{
    public static Integer getA(){
        Integer a = new Integer(10);
        return a;
    }
}
class B{
    public static Integer getB(){
        Integer b = new Integer(10);
        return b;
    }
    public static int get(){
        return 10;
    }
}

public class Test {
    public static void main(String[] args) {
        Object o = new Object();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (o){
                    try {
                        o.wait(3000);
                        System.out.println("唤醒了");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
        System.out.println("main");
        synchronized (o){
            o.notify();
        }
    }

}
