package 定时器;

import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

//描述任务
class MyTask implements Comparable<MyTask>{
    //任务具体的内容
    private Runnable runnable;
    //任务执行的时间戳
    private long time;

    //delay为时间间隔，不是具体的时间戳
    public MyTask(Runnable runnable, long delay){
        this.runnable = runnable;
        this.time = System.currentTimeMillis()+delay;
    }

    @Override
    public int compareTo(MyTask o) {
        return (int) (this.time-o.time);
    }

    public void run(){
        runnable.run();
    }


    public long getTime() {
        return time;
    }

}


public class MyTimer {

    private PriorityBlockingQueue<MyTask> p = new PriorityBlockingQueue<>();
    //创建一个对象用于加锁
    private Object locker = new Object();
    public void schedule(Runnable runnable, long delay){
        MyTask task = new MyTask(runnable, delay);
        p.put(task);
        //插入任务，可能执行时间已经过了，需要唤醒等待的线程进行判断是否执行
        synchronized (locker){
            locker.notify();
        }
    }

    public MyTimer(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        MyTask task = p.take();
                        if(task.getTime() > System.currentTimeMillis()){
                            p.put(task);
                            //当执行时间没到时，没必要一直进行判断，比较耗费CPU
                            //所以等待一定时间
                            synchronized (locker){
                                locker.wait(task.getTime()-System.currentTimeMillis());
                            }
                        }else {
                            task.run();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();
    }

    public static void main(String[] args) {
        MyTimer timer = new MyTimer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello timer");
            }
        }, 3000);
        System.out.println("main");
    }
}
