package 定时器;

import java.util.Timer;
import java.util.TimerTask;

public class Demo {
    public static void main(String[] args) {
        //Timer内部是专门有线程来执行我们注册的任务,这个线程在执行完一个任务还会等待别的任务执行
        Timer timer = new Timer();
        //schedule(任务，多久后执行任务)
        //TimerTask是一个抽象类，实现了Runnable接口
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello timer");
            }
        }, 3000);

        System.out.println("main");
    }
}
