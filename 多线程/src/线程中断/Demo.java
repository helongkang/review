package 线程中断;

public class Demo {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            while(!Thread.currentThread().isInterrupted()){
                System.out.println("执行中");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("执行收尾操作");
                    break;
                }
            }
        });
        t.start();
        Thread.sleep(3000);
        t.interrupt();
    }
}
