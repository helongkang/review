package 线程池;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class MyThreadPool {
    //使用阻塞队列保存任务
    private BlockingDeque<Runnable> q = new LinkedBlockingDeque<>();

    //创建线程池的时候就从阻塞队列中拿任务执行
    public MyThreadPool(int n){
        for(int i = 0;i < n;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true){
                        Runnable task = null;
                        try {
                            task = q.take();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        task.run();
                    }
                }
            }).start();
        }
    }

    //提交任务
    public void submit(Runnable runnable){
        try {
            q.put(runnable);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) throws InterruptedException {
        MyThreadPool myThreadPool = new MyThreadPool(5);
        while(true){
            myThreadPool.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName());
                }
            });
            Thread.sleep(1000);
        }
    }
}
