package 生产者消费者模型;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public class Demo {

    public static void main(String[] args) {
        BlockingDeque<Integer> q = new LinkedBlockingDeque<>();

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
               while(true){
                   try {
                       q.put(1);
                       System.out.println("生产了1个");
                       Thread.sleep(1000);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
               }
            }
        });


        Thread consumer = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        q.take();
                        System.out.println("消费了1个");
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        producer.start();
        consumer.start();
    }
}
