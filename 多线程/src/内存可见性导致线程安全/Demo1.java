package 内存可见性导致线程安全;

class Temp{
    public int flag = 0;
}
public class Demo1 {
    static Object o = new Object();

    public static void main(String[] args) throws InterruptedException {

        Temp t = new Temp();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (Demo1.class){
                    while(true){
                        if(t.flag != 0){
                            break;
                        }

                    }
                }

                System.out.println("循环结束");
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized (o){
                        Thread.sleep(3000);
                        t.flag = 3;
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        t2.start();
    }
}
