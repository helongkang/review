package 内存可见性导致线程安全;

import java.util.Scanner;

public class Demo {

    private static volatile int flag = 0;
    public static void main(String[] args) {
        Thread t = new Thread(() -> {
            while(flag == 0){

            }
            System.out.println("t线程执行完毕");
        });
        t.start();

        Scanner sc = new Scanner(System.in);
        flag = sc.nextInt();
        System.out.println("main线程执行完毕");
    }
}
