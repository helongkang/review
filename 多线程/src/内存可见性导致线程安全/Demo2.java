package 内存可见性导致线程安全;

public class Demo2 {
    static class User{
        int age;
        int count;
        public synchronized void add1(){
            while(true){
                age++;
            }
        }
        public synchronized void add2(){
            count++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        User user = new User();
        Thread t1 = new Thread(() -> {
            user.add1();
        });
        Thread t2 = new Thread(() -> {
            for(int i = 0;i < 50;i++){
                user.add2();
            }
            System.out.println(user.count);
        });
        t1.start();
        Thread.sleep(1000);
        t2.start();
    }
}
