package 内存可见性导致线程安全;

public class Demo3 {
    public static int a = 10;
    public static void main(String[] args) throws InterruptedException {
       Thread t = new Thread(new Runnable() {
           @Override
           public void run() {
               synchronized (Demo3.class){
                   while(a==10){
                       try {
                           Thread.sleep(500);
                       } catch (InterruptedException e) {
                           e.printStackTrace();
                       }
                   }
                   System.out.println("结束");
               }
           }
       });
       t.start();
       Thread.sleep(3000);
       a = 20;
    }
}
