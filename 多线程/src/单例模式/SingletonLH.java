package 单例模式;

//懒汉模式
public class SingletonLH {

    //加volatile的原因：禁止指令重排序
    private static volatile SingletonLH singletonLH = null;

    private SingletonLH(){}

    public static SingletonLH getSingletonLH(){
        if(singletonLH == null){
            synchronized (SingletonLH.class){
                if(singletonLH == null){
                    singletonLH = new SingletonLH();
                }
            }
        }
        return singletonLH;
    }
}
