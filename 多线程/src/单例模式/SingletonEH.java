package 单例模式;

//饿汉模式
public class SingletonEH {
    private static SingletonEH singletonEH = new SingletonEH();

    //为了防止不小心new了这个SingletonEH,所构造方法访问权限为private
    private SingletonEH(){

    }
    //提供一个方法可以让外边能够拿到这个实例
    public static SingletonEH getSingletonEH(){
        return singletonEH;
    }
}
