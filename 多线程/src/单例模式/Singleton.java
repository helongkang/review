package 单例模式;

public class Singleton {
    private static volatile Singleton singleton = null;

    private Singleton(){}; //保证外部不能new实列

    public static Singleton getSingleton(){
        if(singleton == null){
            synchronized (Singleton.class){
                if(singleton == null){
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
