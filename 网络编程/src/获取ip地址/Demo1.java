package 获取ip地址;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Demo1 {
    public static void main(String[] args) throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getByName("2.2.5.101");
        InetAddress inetAddress1 = InetAddress.getLocalHost(); //获取本地主机地址
        System.out.println(inetAddress1);
        String hostName = inetAddress.getHostName(); //获取IP地址对应的主机名/域名
        String hostAddress = inetAddress.getHostAddress(); //获取原始ip地址
        System.out.println(hostName);
        System.out.println(hostAddress);
    }
}
