import com.beans.Student;
import com.beans.UserController2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App1 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        Student student = context.getBean("student1",Student.class);
//        System.out.println(student);
        UserController2 userController2 = context.getBean(UserController2.class);
        userController2.say();
    }
}
