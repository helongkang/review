import com.beans.Student;
import com.beans.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        User user = context.getBean("user",User.class);
//        user.hello();

        //Student student = context.getBean("student1",Student.class);
        //Student student = context.getBean("student",Student.class);
        User user = context.getBean("user",User.class);
        user.hello();
    }
}
