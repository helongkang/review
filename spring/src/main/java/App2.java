import com.beans.Student;
import com.beans.UserController1;
import com.beans.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        UserController1 userController1 = context.getBean(UserController1.class);
//        userController1.sayHello();
        UserService userService = context.getBean(UserService.class);
        userService.say();
    }
}
