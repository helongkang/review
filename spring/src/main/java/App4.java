import com.beans.Animal;
import com.beans.AnimalController1;
import com.beans.AnimalController2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App4 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        AnimalController1 animalController1 = context.getBean(AnimalController1.class);
        animalController1.getAnimal();

        System.out.println("---------");

        AnimalController2 animalController2 = context.getBean(AnimalController2.class);
        animalController2.getAnimal();
    }
}
