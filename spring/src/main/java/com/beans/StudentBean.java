package com.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class StudentBean {

    @Bean //只使用@Bean注解无法将对象存储到容器中,必须配合五大类注解
    public Student student1(){
        Student s1 = new Student();
        s1.setName("张三");
        s1.setAge(20);
        return s1;
    }

    @Bean(name = "student") //起名字，bean name此时为student
    public Student student2(){
        Student s1 = new Student();
        s1.setName("李四");
        s1.setAge(20);
        return s1;
    }
}
