package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController2 {
    private Student student;

    @Autowired
    public void setStudent(Student student) {
        this.student = student;
    }

    public void say(){
        System.out.println(student);
    }
}
