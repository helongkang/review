package com.beans;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

public class UserService2 {
    @Resource(name = "student1")
    private Student student;

    public void say(){
        System.out.println(student);
    }
}
