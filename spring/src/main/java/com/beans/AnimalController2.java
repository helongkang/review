package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class AnimalController2 {

    @Autowired
    private Animal animal;

    public void getAnimal(){
        System.out.println(animal);
    }
}
