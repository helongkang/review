package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private Student student;

    @Autowired
    public UserService(Student student1){
        this.student = student1;
    }
    public void say(){
        System.out.println(student);
    }
}
