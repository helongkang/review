package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class AnimalController1 {

    @Autowired
    private Animal animal;

    public void getAnimal(){
        Animal animal = this.animal;
        animal.setName("大象");
        System.out.println(animal);
    }
}
