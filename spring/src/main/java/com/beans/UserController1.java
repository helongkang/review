package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController1 {

    @Autowired
    private Student student;

    public void sayHello(){
        System.out.println(student);
    }
}
