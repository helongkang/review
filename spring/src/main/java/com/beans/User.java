package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class User {
    @Autowired
    private Student student;

    public void hello(){
        System.out.println(student);
    }
}
