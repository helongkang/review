import com.beans.Dog;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.security.cert.X509Certificate;

public class App {
    public static void main(String[] args) {
        //ApplicationContext context = new ClassPathXmlApplicationContext("spring-config1.xml");
        //Dog dog = (Dog)context.getBean("dog");
        //Dog dog = context.getBean(Dog.class);

        BeanFactory factory = new XmlBeanFactory(new ClassPathResource("spring-config1.xml"));
        Dog dog = factory.getBean("dog",Dog.class);
        dog.say("旺财");
    }
}
