import com.beans.UserController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application1 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //bean name为类名改写为小驼峰方式
        UserController userController = context.getBean("userController",UserController.class);
        userController.sayHello();
    }
}
