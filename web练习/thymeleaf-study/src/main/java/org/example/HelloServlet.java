package org.example;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //创建模板引擎，用于最终完成最终页面渲染工作
        TemplateEngine engine = new TemplateEngine();
        //创建渲染网页模板的解析器
        ServletContextTemplateResolver resolver = new ServletContextTemplateResolver(getServletContext());
        resolver.setCharacterEncoding("utf-8");//设置渲染时编码
        resolver.setPrefix("/WEB-INF/templates/");//前缀
        resolver.setSuffix(".html");//后缀
        //将解析器绑定到模板引擎中
        engine.setTemplateResolver(resolver);

        //创建一个web上下文（里面有一个map结构，存放键值对）
        WebContext wc = new WebContext(req,resp,getServletContext());
        //设置一个键值对数据，键为message(模板中的变量)，值为好好学习(要渲染的值),
        wc.setVariable("message","好好学习");
        wc.setVariable("url1","http://www.baidu.com");
        wc.setVariable("url2","http://www.sogou.com");
        wc.setVariable("isLogin",true);
        wc.setVariable("noLogin",false);
        wc.setVariable("users",Arrays.asList(
                new User("张三",21),
                new User("李四",23),
                new User("王五",28)
        ));

        //模板引擎渲染网页模板，第一个参数为模板名称，第二个参数为web上下文
        //根据模板解析器设置的前缀+模板名称+后缀为模板路径，查找到模板，再组织模板内容+数据
        //返回值就是渲染后的网页字符串
        String html = engine.process("hello",wc);

        resp.setContentType("text/html; charset=utf-8");//设置响应编码
        resp.getWriter().write(html);
    }
    private static class User{
        private String name;
        private int age;

        public User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }
}
