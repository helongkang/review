package org.example;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/test")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext sc = req.getServletContext();
        WebContext wc = new WebContext(req,resp,sc);
        wc.setVariable("content","写一篇博客");
        TemplateEngine engine = (TemplateEngine) sc.getAttribute("engine");
        String html = engine.process("test",wc);
        resp.setContentType("text/html; charset=utf-8");
        resp.getWriter().write(html);
    }
}
