package com.example.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

@RestController
public class TestController {

    @RequestMapping("/img")
    public ResponseEntity<byte[]> getImg() throws IOException {
        FileOutputStream fos = new FileOutputStream("D:\\photo\\test2.jpg");
        File file = new File("D:\\photo\\11.jpg");
        byte[] bytes = Files.readAllBytes(file.toPath());
        return ResponseEntity.ok(bytes);
    }

    @RequestMapping("/music")
    public ResponseEntity<byte[]> getMusic() throws IOException {
        File file = new File("D:\\音乐\\下载\\红日.mp3");
        byte[] bytes = Files.readAllBytes(file.toPath());
        return ResponseEntity.ok(bytes);
    }

    @RequestMapping("/video")
    public ResponseEntity<byte[]> getVideo() throws IOException {
        File file = new File("D:\\音乐\\下载\\MV\\王心凌 - 梦的光点.mp4");
        byte[] bytes = Files.readAllBytes(file.toPath());
        return ResponseEntity.ok(bytes);
    }
}
