package com.example.demo.config;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

//返回json格式数据，将@Controller与@ResponseBody注解合成一个注解@RestController
@RestControllerAdvice
public class MyException {

    @ExceptionHandler(Exception.class)
    public Object handler(Exception e){
        Map<String,Object> map = new HashMap<>();
        map.put("state",-1);
        map.put("data",null);
        map.put("msg",e.getMessage());
        return map;
    }
}
