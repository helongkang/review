package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).
                addPathPatterns("/**"). //拦截所有url
                excludePathPatterns("/user/login"). //排除登陆接口
                excludePathPatterns("/user/register"). //排除注册接口
                excludePathPatterns("/login.html").
                excludePathPatterns("/register.html").
                //排除所有的静态页面
                excludePathPatterns("/**/*.js").
                excludePathPatterns("/**/*.css").
                excludePathPatterns("/**/*.jpg");
    }

//    @Override
//    public void configurePathMatch(PathMatchConfigurer configurer) {
//        //c -> true意思是给所有的controller都添加api前缀
//        configurer.addPathPrefix("api",c -> true);
//    }
}
