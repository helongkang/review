package com.example.demo.config;


import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        if(session != null && session.getAttribute("user") != null){
            return true;
        }
        //当代码进行到这一步时，说明拦截未通过，防止前端显示空白，重定向到登陆页面
        //response.sendRedirect("/login.html");
        return false;
    }
}
