package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/login")
    public String login(String username, String password, HttpServletRequest req){
        if(username==null || password==null){
            return "请输入用户名和密码";
        }
        if(username.equals("abc") && password.equals("123")){
            HttpSession session = req.getSession(true);
            session.setAttribute("user","user");
            return "登陆成功";
        }
        return "登陆失败";
    }

    @RequestMapping("/content")
    public String content(){
        return "已经登陆成功";
    }

    @RequestMapping("/test")
    public String test(){
        return "hello";
    }
}
