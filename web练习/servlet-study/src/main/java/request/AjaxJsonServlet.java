package request;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

@WebServlet("/ajax-servlet")
public class AjaxJsonServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8"); //获取请求body的数据，先设置编码
        InputStream is = req.getInputStream();//获取请求正文的数据，不管content-type是啥类型，都可以获取
//        InputStreamReader isr = new InputStreamReader(is,"utf-8");
//        BufferedReader br = new BufferedReader(isr);
//        StringBuilder sb = new StringBuilder();
//        String str;
//        while((str=br.readLine())!=null){
//            sb.append(str);
//        }
//        System.out.println(sb);
//        int len = req.getContentLength();
//        byte[] bytes = new byte[len];
//        is.read(bytes);
//        System.out.println(new String(bytes,"utf-8"));
        //以上是获取整个json字符串，使用不方便
        //借助第三方库jackson，可以将Java对象和json字符串相互转换
        ObjectMapper mapper = new ObjectMapper();
//        Map json = mapper.readValue(is,Map.class); //任何json格式都可以转换为Map
        //Map使用也不方便，因为要使用指定的字符串键获取值
        User user = mapper.readValue(is,User.class);
        System.out.println(user);
    }
    //json转java对象，需要键的名称和成员变量名一样
    static class User{
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "User{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }
}
