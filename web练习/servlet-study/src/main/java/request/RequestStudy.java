package request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/request")
public class RequestStudy extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取请求协议名
        System.out.println("协议名："+req.getProtocol());
        //获取请求方法
        System.out.println("方法名："+req.getMethod());
        System.out.println("应用上下文路径/应用名："+req.getContextPath());
        System.out.println("资源路径："+req.getServletPath());
        System.out.println("请求头中键为host的值："+req.getHeader("Host"));
        //重点
        //通过键获取值：1.queryString 2.表单格式
        //注意：http请求时，url会进行编码，要获得queryString中的中文，特殊字符，就需要解码
        //body中的内容也是有编码格式的，需要设置才能正常显示
        req.setCharacterEncoding("utf-8");
        System.out.println("username："+req.getParameter("username"));
        System.out.println("password："+req.getParameter("password"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
