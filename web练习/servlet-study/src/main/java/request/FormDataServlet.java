package request;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/form-data")
@MultipartConfig  //form-data格式，需要使用这个注解
public class FormDataServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //req.getParameter也能获取到form-data中的简单数据
        req.setCharacterEncoding("utf-8");
        System.out.println("username："+req.getParameter("username"));
        System.out.println("password："+req.getParameter("password"));
        //req.getParameter获取文件，获取不到
        //form-data中上传的文件，需要通过getPart来获取
        System.out.println("head："+req.getParameter("head"));
        //getPart也可以获取简单类型的数据，但是还需要Part的API来获取内容，比较复杂
        Part head = req.getPart("head");
        //获取文件的二进制数据，转换为字符串打印
        InputStream is = head.getInputStream();
        byte[] bytes = new byte[is.available()];
        is.read(bytes);
        System.out.println(new String(bytes,"utf-8")); //第二个参数为编码格式
    }
}
