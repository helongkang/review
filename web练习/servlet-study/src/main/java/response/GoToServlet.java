package response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/goto")
public class GoToServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");
        if(type.equals("1")){
//            resp.setStatus(301);
//            resp.setHeader("Location","hello.html");
            resp.sendRedirect("hello.html"); //以上代码的简化
        }else if(type.equals("2")){
            req.getRequestDispatcher("hello.html").forward(req,resp);
        }
    }
}
