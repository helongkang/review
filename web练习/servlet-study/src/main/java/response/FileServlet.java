package response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

@WebServlet("/file")
public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OutputStream ops = resp.getOutputStream();
        String type = req.getParameter("type");
        File file = null;
        if("photo".equals(type)){
            file = new File("D:\\photo\\6.jpg");
            resp.setContentType("image/jpeg");
        }else if("music".equals(type)){
            file = new File("D:\\音乐\\下载\\梦的光点.mp3");
            resp.setContentType("audio/mp3");
        }
        byte[] bytes = Files.readAllBytes(file.toPath());
        resp.setContentLength(bytes.length);
        ops.write(bytes);
    }
}
