package response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/html")
public class HTMLResponse extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        String type = req.getParameter("type");
        PrintWriter pw = resp.getWriter();
        if("1".equals(type)){
            pw.println("<h3>获取简单html成功</h3>");
        }else if("2".equals(type)){
            String username = req.getParameter("username");
            pw.println("<p>"+"欢迎您,"+username+"</p>");
        }
    }
}
