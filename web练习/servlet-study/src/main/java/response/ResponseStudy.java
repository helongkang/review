package response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/response")
public class ResponseStudy extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String status = req.getParameter("status");
        //如果需要设置才设置
        if(status != null){
            resp.setStatus(Integer.parseInt(status));
            resp.setCharacterEncoding("utf-8");
            resp.getWriter().write(status);
        }
        //设置响应头的键值对，可以是自定义的
        //响应状态码是301，302，307，响应头有Location字段，才是重定向
        resp.setHeader("Location","http://www/baidu.com");
        resp.setHeader("username","张三");
    }
}
