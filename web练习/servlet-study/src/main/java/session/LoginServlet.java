package session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if("abc".equals(username) && "123".equals(password)){
            //为登陆成功的用户创建一个session
            //tomcat启动时维护了一个数据结构Map<String,Session>，用来保存多个用户的会话信息，键为sessionId（随机字符串）
            //获取当前请求的session，如果没有就创建一个，并放到Map结构中
            HttpSession session = req.getSession(true);
            //session本身也是一个键值对，可以保存多组键值对信息
            session.setAttribute("u",username);
            //以上session的代码会设置一个Set-Cookie：jsessionid=随机字符串
            resp.sendRedirect("hello.html");
        }else {
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("登陆失败");
        }
    }
}
