package session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/sensitive")
public class SensitiveServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        //获取当此请求的session（此时可能登陆，也可能没有登陆）
        //如果获取不到，返回null
        HttpSession session = req.getSession(false);
        if(session != null){
            String username = (String)session.getAttribute("u");
            if(username != null){
                resp.getWriter().write("用户已登陆，允许访问");
                return;
            }
        }
        resp.getWriter().write("用户未登录，不允许访问");
    }
}
