
    function sendGet(){
        ajax({
            method: "GET",
            url: "hello",
            callback: function(status,responseText){
                console.log("hello GET");
            }
        });
    }
    function sendPost(){
        ajax({
            method: "POST",
            url: "hello",
            callback: function(status,responseText){
                console.log("hello POST");
            }
        });
    }
    function ajax(args){
        let xhr = new XMLHttpRequest();
        //设置回调函数
        xhr.onreadystatechange = function(){
            //4：客户端接收到服务端响应
            if(xhr.readyState == 4){
                //回调函数可能会使用响应的内容,作为传入参数
                args.callback(xhr.status,xhr.responseText);
            }
        }
        xhr.open(args.method,args.url);
        //如果args中contentType有内容，就设置Content-Type请求头
        if (args.contentType) {//js中if可以判断是否有值
            xhr.setRequestHeader("Content-Type", args.contentType);
        }
        //如果args中body有内容，设置body请求正文
        if(args.body){
            xhr.send(args.body);
        }else {
            xhr.send();
        }
    }