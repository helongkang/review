package org.example.api;

import org.example.dao.MessageDao;
import org.example.model.Message;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/message")
public class MessageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        TemplateEngine engine = (TemplateEngine) getServletContext().getAttribute("engine");
        WebContext wc = new WebContext(req,resp,getServletContext());
        wc.setVariable("messages",MessageDao.selectMessage());
        String html = engine.process("表白墙",wc);
        resp.getWriter().write(html);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String from = req.getParameter("from");
        String to = req.getParameter("to");
        String content = req.getParameter("content");
        Message m = new Message();
        m.setFrom(from);
        m.setTo(to);
        m.setContent(content);
        //messages.add(m);
        MessageDao.insertMessage(m);
        doGet(req,resp);
    }
}
