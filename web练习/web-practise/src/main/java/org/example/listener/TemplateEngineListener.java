package org.example.listener;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class TemplateEngineListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        TemplateEngine engine = new TemplateEngine();
        ServletContext sc = sce.getServletContext();
        ServletContextTemplateResolver resolver = new ServletContextTemplateResolver(sc);
        resolver.setPrefix("/WEB-INF/templates/");//前缀
        resolver.setSuffix(".html");//后缀
        resolver.setCharacterEncoding("utf-8");//设置渲染编码
        engine.setTemplateResolver(resolver);
        sc.setAttribute("engine",engine);//将引擎设置到ServletContext中
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
