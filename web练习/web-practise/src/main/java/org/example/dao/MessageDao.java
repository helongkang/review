package org.example.dao;

import org.example.model.Message;
import org.example.util.DBUtil;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//dao是专门做数据库操作的
public class MessageDao {
    //将一个message对象插入到数据库对应的message表中
    public static void insertMessage(Message m){
        //jdbc步骤:
        //1.获取数据库连接
        //2.获取操作命令对象
        //3.执行sql
        //4.处理结果集
        //5.释放资源
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = DBUtil.getConnection();
            String sql = "insert into message(`from`,`to`,content) values(?,?,?)";
            ps = c.prepareStatement(sql);
            ps.setString(1,m.getFrom());
            ps.setString(2,m.getTo());
            ps.setString(3,m.getContent());
            int n = ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("插入message出错",e);
        } finally {
            DBUtil.close(c,ps);
        }
    }

    //查询所有的消息，放在集合中返回
    public static List<Message> selectMessage(){
        List<Message> list = new ArrayList<>();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = DBUtil.getConnection();
            String sql = "select * from message";
            ps = c.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                Message m = new Message();
                String from = rs.getString("from");
                String to = rs.getString("to");
                String content = rs.getString("content");
                m.setFrom(from);
                m.setTo(to);
                m.setContent(content);
                list.add(m);
            }
        } catch (SQLException e) {
            throw new RuntimeException("查询message出错",e);
        } finally {
            DBUtil.close(c,ps,rs);
        }
        return list;
    }
    //插入的测试
    @Test
    public void testInsert(){
        Message m = new Message();
        m.setFrom("hlk");
        m.setTo("fs");
        m.setContent("521");
        insertMessage(m);
    }
    //查询的测试
    @Test
    public void testSelect(){
        List<Message> list = selectMessage();
        System.out.println(list);
    }
}
