package request;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/ajax-servlet")
public class AjaxServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        //getInputStream可以获取任意格式的body，但是如果用于表单格式还要解析多组键值对
        //太麻烦，所以一般用于获取json格式的请求body
        InputStream is = req.getInputStream();
        //借助第三方库jackson，可以将Java对象和json字符串相互转换
        ObjectMapper mapper = new ObjectMapper();
        //json转java对象，需要json的键和java对象的成员变量相对应
        User user = mapper.readValue(is,User.class);
        System.out.println(user);
    }
    //必须提供getter和setter方法
    static class User{
        private String username;
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "User{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }
}
