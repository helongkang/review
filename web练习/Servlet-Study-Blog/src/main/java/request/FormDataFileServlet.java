package request;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/form-data-file")
@MultipartConfig
public class FormDataFileServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //getPart也可以获取简单类型的数据，但是还需要Part的API来获取内容，比较复杂
        Part head = req.getPart("head");
        //获取文件的二进制数据，转换为字符串打印
        InputStream is = head.getInputStream();
        byte[] bytes = new byte[is.available()];
        is.read(bytes);
        System.out.println(new String(bytes,"utf-8")); //第二个参数为编码格式
    }
}
