package login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/sensitive")
public class SensitiveServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        //如果获取不到就为null
        HttpSession session = req.getSession(false);
        if(session != null){
            String username = (String)session.getAttribute("user");
            if(username != null){ //检测username是否为空
                resp.getWriter().write("用户已经登陆，允许访问");
                return;
            }
        }
        resp.getWriter().write("用户未登录，不允许访问");
    }
}
