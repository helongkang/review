package login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if("abc".equals(username) && "123".equals(password)){
            //获取session信息，true表示如果获取不到就创建一个
            HttpSession session = req.getSession(true);
            session.setAttribute("user",username);
            //登陆成功跳转到success.html
            resp.sendRedirect("success.html");
        }else {
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("登陆失败");
        }
    }
}
