package response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/html-response")
public class HTMLServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        String type = req.getParameter("type");
        if("1".equals(type)){
            resp.getWriter().write("<p>"+"简单HTML"+"</p>");
        }else if("2".equals(type)){
            String name = req.getParameter("username");
            resp.getWriter().write(name+"欢迎您！");
        }
    }
}
