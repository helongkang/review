package response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/response")
public class ResponseServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String status = req.getParameter("status");
        if(status != null){
            resp.setStatus(Integer.parseInt(status));
            resp.setCharacterEncoding("utf-8");
            resp.getWriter().write(status);
        }
        resp.setHeader("Location","http://www.baidu.com");
        resp.setHeader("name","panda");
    }
}
