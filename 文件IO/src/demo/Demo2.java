package demo;

import java.io.FileOutputStream;
import java.io.IOException;

public class Demo2 {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("dir/test.txt");
        //一个字节一个字节的写入
//        fos.write(97);
//        fos.write(98);
//        fos.write(99);

        //一次写入一个字节数组
        byte[] bytes = {97,98,99,101};
        fos.write(bytes);
        fos.close();
    }
}
