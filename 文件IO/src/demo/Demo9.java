package demo;

import java.io.*;

//字符流
public class Demo9 {
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(new FileInputStream("dir/test.txt"),"utf-8");
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("dir/test2.txt"),"utf-8");
        int len;
        char[] chars = new char[1024]; //字符流读到字符数组中
        while((len = isr.read(chars)) != -1){
            osw.write(chars,0,len);
        }
        osw.flush();
        osw.close();
        isr.close();
    }
}
