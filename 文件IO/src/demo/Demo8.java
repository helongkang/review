package demo;

import java.io.FileInputStream;
import java.io.IOException;

public class Demo8 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("dir/文本.txt");
        int len;
        byte[] bytes = new byte[1024];
        while((len = fis.read(bytes)) != -1){
            String s = new String(bytes,0,len,"GBK");
            System.out.println(s);
        }
    }
}
