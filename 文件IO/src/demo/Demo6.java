package demo;

import java.io.*;

public class Demo6 {
    public static void main(String[] args) throws IOException {
//        FileOutputStream fos = new FileOutputStream("dir/test.txt");
//        BufferedOutputStream bos = new BufferedOutputStream(fos);
//        bos.write("硕姐真好".getBytes());
//        bos.close();
//        fos.close();
        FileInputStream fis = new FileInputStream("dir/test.txt");
        BufferedInputStream bis = new BufferedInputStream(fis);
        int len;
        byte[] bytes = new byte[1024];
        while((len = bis.read(bytes)) != -1){
            System.out.println(new String(bytes,0,len));
        }
    }
}
