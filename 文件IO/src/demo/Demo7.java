package demo;

import java.io.*;

public class Demo7 {
    public static void main(String[] args) throws IOException {
        long s3 = System.currentTimeMillis();
        method3();
        long e3 = System.currentTimeMillis();
        System.out.println("正常+一个一个字节："+(e3-s3));
        long s2 = System.currentTimeMillis();
        method2();
        long e2 = System.currentTimeMillis();
        System.out.println("正常+byte数组："+(e2-s2));
        long s1 = System.currentTimeMillis();
        method1();
        long e1 = System.currentTimeMillis();
        System.out.println("缓冲流："+(e1-s1));
    }
    public static void method1() throws IOException {
        FileInputStream fis = new FileInputStream("dir/视频.mp4");
        FileOutputStream fos = new FileOutputStream("dir/视频1.mp4");
        BufferedInputStream bis = new BufferedInputStream(fis);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        byte[] bytes = new byte[1024];
        int len;
        while((len = bis.read(bytes)) != -1){
            bos.write(bytes,0,len);
        }
    }
    public static void method2() throws IOException {
        FileInputStream fis = new FileInputStream("dir/视频.mp4");
        FileOutputStream fos = new FileOutputStream("dir/视频2.mp4");
        byte[] bytes = new byte[1024];
        int len;
        while((len = fis.read(bytes)) != -1){
            fos.write(bytes,0,len);
        }
    }
    public static void method3() throws IOException {
        FileInputStream fis = new FileInputStream("dir/视频.mp4");
        FileOutputStream fos = new FileOutputStream("dir/视频3.mp4");
        int len;
        while((len = fis.read()) != -1){
            fos.write(len);
        }
    }
}
