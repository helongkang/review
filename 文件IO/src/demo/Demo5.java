package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo5 {
    public static void main(String[] args) throws IOException {
        //复制文本
//        FileInputStream fis = new FileInputStream("dir/test.txt");
//        FileOutputStream fos = new FileOutputStream("dir/test1.txt");
//        int n;
//        while((n = fis.read()) != -1){
//            fos.write(n);
//        }
//        fos.close();
//        fis.close();
        //复制图片
        FileInputStream fis = new FileInputStream("dir/图片.jpg");
        FileOutputStream fos = new FileOutputStream("dir/图片1.jpg");
        byte[] bytes = new byte[1024];
        int len;
        while((len = fis.read(bytes)) != -1){
            fos.write(bytes,0,len);
        }
        fos.close();
        fis.close();
    }
}
