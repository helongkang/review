package demo;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//更便捷的字符流
public class Demo10 {
    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("dir/test.txt");
        FileWriter fw = new FileWriter("dir/test3.txt");
        int len;
        char[] chars = new char[1024];
        while((len = fr.read(chars)) != -1){
            fw.write(chars,0,len);
        }
        fw.flush();
        fw.close();
        fr.close();
    }
}
