package demo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Demo3 {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("dir/test.txt",true); //true表示追加写
        String str = "好好学习，进大厂";
        fos.write(str.getBytes());
        for(int i = 0;i < 5;i++){
            fos.write("硕妹妹".getBytes());
            fos.write("\n".getBytes());
        }
        fos.close();
    }
}
