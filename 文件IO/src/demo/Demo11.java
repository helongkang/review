package demo;

import java.io.*;

//缓冲字符流复制java文件
public class Demo11 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("src/demo/Demo1.java"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("dir/test4.java"));
        String s;
        while((s = br.readLine()) != null){ //读一行
            bw.write(s);
            bw.newLine(); //换行
        }
        bw.close();
        br.close();
    }
}
