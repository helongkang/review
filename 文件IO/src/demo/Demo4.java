package demo;

import java.io.FileInputStream;
import java.io.IOException;

public class Demo4 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("dir/test.txt");
//        int n;
//        while((n = fis.read()) != -1){
//            System.out.print(n);
//        }
        byte[] bytes = new byte[5];
        int n;
        while((n = fis.read(bytes)) != -1){
            String str = new String(bytes,0,n);
            System.out.print(str);
        }
    }
}
