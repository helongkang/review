package IO流;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class BufferedOutStreamTest {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test1.txt",true);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        bos.write(new byte[]{1,2,3,4});
        bos.write("我要吃饭".getBytes());
        bos.flush();
        bos.close();
        fos.close();
    }
}
