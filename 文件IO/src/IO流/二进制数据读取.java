package IO流;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class 二进制数据读取 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test.txt");
        BufferedInputStream bis = new BufferedInputStream(fis);
        byte[] bytes = new byte[1024];
        int len;
        while((len = bis.read(bytes)) != -1){
            String str = new String(bytes,0,len);
            System.out.println(str);
        }
        bis.close();
        fis.close();
    }
}
