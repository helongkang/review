package IO流;


import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamTest {
    public static void main(String[] args) throws IOException {

//        //一个字节一个字节的读
//        int b;
//        while ((b = fis.read()) != -1) {
//            System.out.println(b);
//        }
//        fis.close();

        //一个字节一个字节得读，但是转换为文本
        try(FileInputStream fis = new FileInputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test.txt");){
            byte[] bytes = new byte[1024];
            int len;//读取得长度
            while((len = fis.read(bytes)) != -1){
                //注意：将字节数组转为字符串的时候，编码格式需要和文本保存的格式相同，否则会发生乱码
                String str = new String(bytes,0,len,"utf-8");//将字节数组0-len范围转为字符串
                System.out.println(str);
            }
        }
    }
}
