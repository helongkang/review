package IO流;

import java.io.*;

public class 文本数据读取 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test.txt");
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        //br.readLine可以一行一行的读取，如果为null，表示读完
        String line;
        while((line = br.readLine()) != null){
            System.out.println(line);
        }
    }
}
