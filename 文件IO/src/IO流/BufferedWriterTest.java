package IO流;

import java.io.*;

public class BufferedWriterTest {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test2.txt",true);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write("你好\n");
        bw.write("世界");
        bw.flush();
        bw.close();
        osw.close();
        fos.close();
    }
}
