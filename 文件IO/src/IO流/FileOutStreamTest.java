package IO流;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutStreamTest {
    public static void main(String[] args) throws IOException {
        //第二个参数，设置为true就是以追加的方式添加内容，设置为false就是修改整个文件
        FileOutputStream fos = new FileOutputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test1.txt",true);
        //fos.write(new byte[]{1,2,3,4}); //这样写字节数据就是乱码
        //写文本
        fos.write("我爱吃饭".getBytes());
        fos.flush();
        fos.close();
    }
}
