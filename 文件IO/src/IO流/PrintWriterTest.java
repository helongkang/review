package IO流;

import java.io.*;

public class PrintWriterTest {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test3.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        PrintWriter pw = new PrintWriter(osw);
        pw.println("你好"); //带换行
        pw.print("世界"); //不换行
        pw.println("你好");
        pw.flush();
        pw.close();
        osw.close();
        fos.close();
    }
}
