package 文件操作;

import java.io.File;
import java.io.IOException;

public class demo {
    public static void main(String[] args) throws IOException {
//        System.out.println(File.separator);
//        //创建文件实例对象，不会创建文件
//        //反斜杠（\）是转义，所以需要两个，也可以写/
//        File file = new File("D:\\photo\\17.jpg");
//        file.mkdir();

//        File file1 = new File("D:\\photo\\11.jpg");
//        System.out.println(file1.getPath());
//        File file2 = new File("D:\\photo\\test.txt");//硬盘不存在的文件
//        File file3 = new File("D:\\photo\\test1");//目录
//        System.out.println(file1.exists());
//        System.out.println(file2.exists());
//        System.out.println(file1.getName());
//        System.out.println(file3.getName());
//        System.out.println(file1.getPath());
//        File file1 = new File("D:\\photo\\11.jpg");//文件
//        System.out.println(file1.isFile());
//        System.out.println(file1.isDirectory());
//        File file3 = new File("D:\\photo\\test1");//目录
//        System.out.println(file3.isDirectory());
//        System.out.println(file3.isFile());
//        File file2 = new File("D:\\photo");
//        File[] files = file2.listFiles();
//        for(File f : files){
//            System.out.print(f+" ");
//        }
//        File file1 = new File("D:\\photo\\test1\\test2");//test1文件存在
//        File file2 = new File("D:\\photo\\test3\\test4");//test3文件不存在
//        boolean r1 = file1.mkdir();
//        boolean r2 = file2.mkdir();
//        boolean r3 = file2.mkdirs();
//        System.out.println(r1);
//        System.out.println(r2);
//        System.out.println(r3);
        File file1 = new File("D:\\photo\\test.txt");//test.txt不存在
        file1.createNewFile();
        file1.delete();
    }
}
