package day20230417;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Demo3 {
    public static void main(String[] args) throws IOException {
        InputStream is = new FileInputStream("./dir/test.txt");
        byte[] bytes = new byte[1024];
        int len;
        while((len = is.read(bytes)) != -1){
            String s = new String(bytes,0,len);
            System.out.println(s);
        }
    }
}
