package day20230417;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo11 {
    public static void main(String[] args) throws IOException {
        try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("./dir/test.txt"))){
            String str = "世界，你好";
            byte[] bytes = str.getBytes();
            bos.write(bytes);
            bos.flush();
        }
    }
}
