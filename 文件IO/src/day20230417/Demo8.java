package day20230417;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Demo8 {
    public static void main(String[] args) throws IOException {
        try(Reader r = new FileReader("./dir/test.txt")){
            char[] c = new char[100];
            int n;
            while((n = r.read(c)) != -1){
                String s = new String(c,0,n);
                System.out.print(s);
            }
        }
    }
}
