package day20230417;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Demo7 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String str = "hello";
        byte[] bytes = str.getBytes("utf-8");
        String s = new String(bytes,"utf-8");
        try(Scanner sc = new Scanner(new FileInputStream("./dir/test.txt"))){
            while(sc.hasNext()){
                System.out.println(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
