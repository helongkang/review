package day20230417;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Demo5 {
    public static void main(String[] args) throws IOException {
        try(OutputStream os = new FileOutputStream("./dir/test.txt",true)){
            os.write(97);
            os.write(98);
            os.write(99);
            os.write(100);
        }
    }
}
