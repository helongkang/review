package day20230417;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Demo2 {
    public static void main(String[] args) throws IOException {
        //该文件路径必须存在，因为是读文件
        InputStream is = new FileInputStream("./dir/test.txt");
        //一个字节一个字节读
//        int n;
//        while((n = is.read()) != -1){
//            System.out.println(n);
//        }
        //往字节数组中读
        byte[] bytes = new byte[1024];
        int len;
        while((len = is.read(bytes)) != -1){
            String s = new String(bytes,0,len);
            System.out.println(s);
        }
        is.close();
    }
}
