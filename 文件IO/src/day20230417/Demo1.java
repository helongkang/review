package day20230417;

import java.io.File;
import java.io.IOException;

public class Demo1 {
    public static void main(String[] args) throws IOException {
//        File file = new File("./dir/test.txt"); //此路径不要求真实存在
//        System.out.println(file.getName()); //获取目录或文件名称
//        System.out.println(file.getPath()); //获取文件路径（用啥路径创建File，返回啥路径）
//        System.out.println(file.getAbsoluteFile()); //获取文件绝对路径
//        System.out.println(file.getCanonicalPath()); //获取使用File修饰过的绝对路径
//        System.out.println(file.exists()); //判断目录或文件是否存在
//        System.out.println(file.isFile()); //判断是否是文件
//        System.out.println(file.isDirectory()); //判断是否是目录

        File file = new File("./a/b/c");
        file.mkdirs(); //创建多级目录
        File file1 = new File("./a/b/c/d.txt");
        file1.delete(); //删除文件
    }
}
