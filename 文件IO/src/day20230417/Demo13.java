package day20230417;

import java.io.*;

public class Demo13 {
    public static void method1() throws IOException {
        InputStream is = new FileInputStream("./dir/图片.jpg");
        OutputStream os = new FileOutputStream("./dir/图片复制.jpg");
        int len;
        byte[] bytes = new byte[1024];
        while((len = is.read(bytes)) != -1){
            os.write(bytes,0,len);
        }
        os.flush();
        os.close();
        is.close();
    }
    public static void method2() throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("./dir/图片.jpg"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("./dir/图片复制1.jpg"));
        int len;
        byte[] bytes = new byte[1024];
        while((len = bis.read(bytes)) != -1){
            bos.write(bytes);
        }
        bos.flush();
        bos.close();
        bis.close();
    }
    public static void method3() throws IOException {
        FileReader fr = new FileReader("./dir/server.xml");
        FileWriter fw = new FileWriter("./dir/server1.xml");
        int len;
        char[] c = new char[255];
        while((len = fr.read(c)) != -1){
            fw.write(c,0,len);
        }
        fw.flush();
        fw.close();
        fr.close();
    }
    public static void method4() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("./dir/server.xml"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("./dir/server2.xml"));
        int len;
        char[] c = new char[255];
        while((len = br.read(c)) != -1){
            bw.write(c,0,len);
        }
        bw.flush();
        bw.close();
        br.close();
    }
    public static void main(String[] args) throws IOException {
        long start1 = System.currentTimeMillis();
        method1();
        long end1 = System.currentTimeMillis();
        System.out.println("字节流耗时："+(end1-start1));
        long start2 = System.currentTimeMillis();
        method2();
        long end2 = System.currentTimeMillis();
        System.out.println("缓冲字节流耗时："+(end2-start2));
        long start3 = System.currentTimeMillis();
        method3();
        long end3 = System.currentTimeMillis();
        System.out.println("字符流耗时："+(end3-start3));
        long start4 = System.currentTimeMillis();
        method4();
        long end4 = System.currentTimeMillis();
        System.out.println("缓冲字符流耗时："+(end4-start4));
    }
}
