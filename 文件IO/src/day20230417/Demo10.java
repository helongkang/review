package day20230417;

import java.io.*;

public class Demo10 {
    public static void main(String[] args) throws IOException {
        try(BufferedInputStream bis = new BufferedInputStream(new FileInputStream("./dir/test.txt"))){
            int n;
            byte[] bytes = new byte[1024];
            while((n = bis.read(bytes)) != -1){
                String str = new String(bytes,0,n);
                System.out.print(str);
            }
        }
    }
}
