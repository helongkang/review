package day20230417;

import java.io.*;

public class Demo12 {
    public static void main(String[] args) throws IOException {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter("./dir/test.txt"))){
            String str = "好好学习 找好工作";
            bw.write(str);
            bw.flush();
        }
    }
}
