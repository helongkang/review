package day20230417;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Demo6 {
    public static void main(String[] args) {
        try(OutputStream os = new FileOutputStream("./dir/test.txt")){
            byte[] bytes = {96,97,98,99,100};
            os.write(bytes);
            os.flush(); //刷新操作
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
