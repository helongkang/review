package day20230417;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Demo9 {
    public static void main(String[] args) throws IOException {
        try(Writer w = new FileWriter("./dir/test.txt")){
            w.write(98);
            w.write('\n');
            w.write("hello");
            w.write('\n');
            char[] c = {'w','o','r','l','d'};
            w.write(c);
            w.write('\n');
            w.write("好好学习",0,2);
            w.write('\n');
            w.write(c,0,2);
            w.flush();
        }
    }
}
