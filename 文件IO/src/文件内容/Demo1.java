package 文件内容;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Demo1 {
    public static void main(String[] args) {

//        try(InputStream is = new FileInputStream("D:\\软件\\Git\\review\\文件IO\\dir\\test.txt")){
//            while(true){
//                int b = is.read();
//                if(b == -1){
//                    break;
//                }
//                System.out.println(b);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        try(InputStream is = new FileInputStream("D:\\Java Study\\review\\文件IO\\dir\\test.txt")){
            while(true){
                byte[] bytes = new byte[1024];
                int len = is.read(bytes);
                if(len == -1){
                    break;
                }
                for(int i = 0;i < len;i++){
                    System.out.println(bytes[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
