package 文件内容;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Demo3 {
    public static void main(String[] args) {
        try(Reader r = new FileReader("D:\\Java Study\\review\\文件IO\\dir\\test.txt")){
            while(true){
                char[] chars = new char[1024];
                int len = r.read(chars);
                if(len == -1){
                    break;
                }
//                for(int i = 0;i < len;i++){
//                    System.out.println(chars[i]);
//                }
                String s = new String(chars,0,len);
                System.out.println(s);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
