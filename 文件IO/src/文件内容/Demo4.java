package 文件内容;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Demo4 {
    public static void main(String[] args) {
        //第二个参数为true就可以以追加方式的写文件，不设置默认为false
        try(Writer w = new FileWriter("D:\\Java Study\\review\\文件IO\\dir\\test.txt",true)){
            String s = "好好学习";
            //可以写字符，也可以写字符串
            w.write(s);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
