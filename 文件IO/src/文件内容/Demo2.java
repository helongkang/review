package 文件内容;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Demo2 {
    public static void main(String[] args) {
        //每次按照写的方式打开文件，都会清空原有文件的内容
        try(OutputStream os = new FileOutputStream("D:\\Java Study\\review\\文件IO\\dir\\test.txt")){
//            os.write(97);
//            os.write(98);
//            os.write(99);
            byte[] bytes = {97,98,99};
            os.write(bytes);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
