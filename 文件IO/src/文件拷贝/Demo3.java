package 文件拷贝;

import java.io.*;

public class Demo3 {
    public static void main(String[] args) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("dir/15.jpg"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("dir/test3.jpg"));
        byte[] bytes = new byte[1024];
        int len;
        while((len=bis.read(bytes)) != -1){
            bos.write(bytes,0,len);
        }
        bos.close();
        bis.close();
    }
}
