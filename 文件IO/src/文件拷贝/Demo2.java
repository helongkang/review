package 文件拷贝;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Demo2 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("dir/15.jpg");
        FileOutputStream fos = new FileOutputStream("dir/test2.jpg");
        byte[] bytes = new byte[1024];
        int len;
        while((len = fis.read(bytes)) != -1){
            fos.write(bytes,0,len);//读多少个字节就写多少个字节
        }
        fos.close();
        fis.close();
    }
}
