package 文件拷贝;

import java.io.*;

public class Demo4 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("dir/test.txt"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("dir/test4.txt"));
        char[] chars = new char[10];
        int len;
        while((len=br.read(chars)) != -1){
            bw.write(chars,0,len);
        }
        bw.flush();
    }
}
