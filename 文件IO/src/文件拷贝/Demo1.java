package 文件拷贝;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class Demo1 {
    public static void main(String[] args) throws IOException {
        File file = new File("dir/15.jpg");
        byte[] bytes = Files.readAllBytes(file.toPath());
        FileOutputStream fos = new FileOutputStream("dir/test.jpg");
        fos.write(bytes);
        fos.close();
    }
}
